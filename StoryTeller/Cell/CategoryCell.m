//
//  CategoryCell.m
//  Typogrphy
//
//  Created by NectarBits on 04/01/18.
//  Copyright © 2018 NectarBits. All rights reserved.
//

#import "CategoryCell.h"
static CGSize _extraMargins = {0,0};
@implementation CategoryCell
@synthesize lblCategory;

- (instancetype)initWithFrame:(CGRect)rect {
    
    self = [super initWithFrame:rect];
    
    if (self) {
        lblCategory = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
        lblCategory.clipsToBounds = YES;
        lblCategory.numberOfLines = 1;
        lblCategory.textAlignment = NSTextAlignmentCenter;
//        [lblCategory setTranslatesAutoresizingMaskIntoConstraints:YES];
        [self.contentView addSubview:lblCategory];
        
        _lblSelector = [[UILabel alloc] initWithFrame:CGRectMake(5, rect.size.height-1, rect.size.width-10, 1)];
        _lblSelector.clipsToBounds = YES;
        [_lblSelector setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:_lblSelector];
        
        _imgFont = [[UIImageView alloc] initWithFrame:CGRectMake(rect.size.width/2-10, 5, 25, 25)];
        _imgFont.clipsToBounds = YES;
        _imgFont.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_imgFont];
    }
    
    
    return self;
}

- (CGSize)intrinsicContentSizeOfLabel
{
    CGSize size = [lblCategory intrinsicContentSize];
    
    if (CGSizeEqualToSize(_extraMargins, CGSizeZero))
    {
        // quick and dirty: get extra margins from constraints
        for (NSLayoutConstraint *constraint in self.constraints)
        {
            if (constraint.firstAttribute == NSLayoutAttributeBottom || constraint.firstAttribute == NSLayoutAttributeTop)
            {
                // vertical spacer
                _extraMargins.height += [constraint constant];
            }
            else if (constraint.firstAttribute == NSLayoutAttributeLeading || constraint.firstAttribute == NSLayoutAttributeTrailing)
            {
                // horizontal spacer
                _extraMargins.width += [constraint constant];
            }
        }
    }
    
    // add to intrinsic content size of label
    size.width += _extraMargins.width;
    size.height += _extraMargins.height;
    
    return size;
}

@end
