//
//  StoryTellerCell.h
//  StoryTeller
//
//  Created by harshesh on 27/04/19.
//  Copyright © 2019 nectarbits. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoryTellerCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnDeleteFrame;

@end

