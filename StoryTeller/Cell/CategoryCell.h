//
//  CategoryCell.h
//  Typogrphy
//
//  Created by NectarBits on 04/01/18.
//  Copyright © 2018 NectarBits. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCell : UICollectionViewCell
@property (nonatomic, strong) UILabel *lblCategory;
@property (nonatomic, strong) UILabel *lblSelector;
@property (nonatomic, strong) UIImageView *imgFont;
- (CGSize)intrinsicContentSizeOfLabel;
@end
