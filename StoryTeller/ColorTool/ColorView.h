//
//  ColorView.h
//  StoryTeller
//
//  Created by nectarbits on 5/1/19.
//  Copyright © 2019 nectarbits. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ColorViewDelegate;

@interface ColorView : UIView <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong)UICollectionView *collColorView;
@property (nonatomic, strong)NSMutableArray *colorArr;
@property (nonatomic, weak) id<ColorViewDelegate> delegate;
@property (nonatomic, strong) UIButton *btnDownload;

-(void)setBackgroundColorCategory;

@end

@protocol ColorViewDelegate <NSObject>

-(void)SelectedColor:(UIColor*)color;
-(void)hidecolorView;
@end


NS_ASSUME_NONNULL_END
