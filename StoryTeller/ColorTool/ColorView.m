//
//  ColorView.m
//  StoryTeller
//
//  Created by nectarbits on 5/1/19.
//  Copyright © 2019 nectarbits. All rights reserved.
//

#import "ColorView.h"
#import "StickerCell.h"

@interface ColorView()
{
    NSString *selectedBG;
    
}
@end

@implementation ColorView
@synthesize collColorView,btnDownload,delegate,colorArr;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.clipsToBounds = YES;
        
        self.frame = frame;
    
        UICollectionViewFlowLayout *layout1 = [[UICollectionViewFlowLayout alloc] init];
        [layout1 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        //225
        collColorView =[[UICollectionView alloc] initWithFrame:CGRectMake(0, 40, self.frame.size.width,  self.frame.size.height - 65) collectionViewLayout:layout1];
        [collColorView setDataSource:self];
        [collColorView setDelegate:self];
        
        layout1.minimumLineSpacing = 5.0f;
        layout1.minimumInteritemSpacing = 5.0f;
        [collColorView registerClass:[StickerCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collColorView setBackgroundColor:[UIColor blackColor]];
        [collColorView setShowsVerticalScrollIndicator:NO];
        [self addSubview:collColorView];
        
        UIButton *btnDown = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDown addTarget:self
                    action:@selector(BtnDownPressed:)
          forControlEvents:UIControlEventTouchUpInside];
        btnDown.frame = CGRectMake(0.0, collColorView.frame.origin.y + collColorView.frame.size.height, self.frame.size.width, 25);
        btnDown.backgroundColor = [UIColor blackColor];
        [btnDown setImage:[UIImage imageNamed:@"ic_downArrow"] forState:UIControlStateNormal];
        [self addSubview:btnDown];
        
        
        colorArr = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"Color" ofType:@"plist"]];
        
//        colorArr = [[NSMutableArray alloc] initWithObjects:@"FFFFFF",@"39C3EF",@"F6EE55",@"A7E66E",@"EE8C3A",@"FE7171",@"9C79E4",@"CCCCCC",@"666666",@"333333",@"29ca95",@"dcbb45",@"1CA5FF",@"c67F55",@"7D52AA",@"108dc7",@"ef8e38",@"636363",@"99f2c8",@"89253e",@"FBD3E9",@"BB377D",@"3a6186",@"#727a17",@"c31432",@"240b36",@"ffe259",@"ffa751",@"ff0084",@"33001b", nil];
        
        [collColorView reloadData];
        
    }
    
    return self;
}


//MARK:- CollectionView Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return colorArr.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StickerCell *cell = [collColorView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.backgroundColor=[UIColor lightGrayColor];
    
    [[[cell.imgSticker.layer sublayers] objectAtIndex:0] removeFromSuperlayer];
    cell.imgSticker.image = nil;
    cell.imgSticker.backgroundColor = [self colorWithHexString:[colorArr objectAtIndex:indexPath.row]];
    
    cell.layer.cornerRadius = 4.0f;
    cell.imgSelection.layer.cornerRadius = 4.0f;
    cell.imgSelection.layer.masksToBounds = YES;
    cell.imgSticker.layer.cornerRadius = 4.0f;
    cell.imgSticker.layer.masksToBounds = YES;
    //    cell.layer.shadowRadius  = 4.0f;
    //    cell.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    //    cell.layer.shadowOffset  = CGSizeZero;
    //    cell.layer.shadowOpacity = 1.2f;
    cell.layer.masksToBounds = NO;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(50, 50);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(10, 5, 10, 5);// top, left, bottom, right
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedBG = [colorArr objectAtIndex:indexPath.row];
    [collColorView reloadData];
    
    UIColor *color = [self colorWithHexString:selectedBG];
    
    [delegate SelectedColor:color];
    
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)setBackgroundColorCategory
{
    [collColorView setHidden:NO];
    collColorView.frame = CGRectMake(0, 40, self.frame.size.width, self.frame.size.height - 65);
    [collColorView reloadData];
}

//MARK:- Action Methods
-(void)BtnDownPressed:(UIButton*)sender
{
    NSLog(@"you clicked on button %@", sender.tag);
    [delegate hidecolorView];
}

@end
