//
//  HomeVC.m
//  StoryTeller
//
//  Created by nectarbits on 4/27/19.
//  Copyright © 2019 nectarbits. All rights reserved.
//

#import "HomeVC.h"
#import "StoryTellerVC.h"

@interface HomeVC ()

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//MARK:- Action Method
-(IBAction)btnCreateAction:(id)sender{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    StoryTellerVC* controller = [storyBoard instantiateViewControllerWithIdentifier:@"StoryTellerVC"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
