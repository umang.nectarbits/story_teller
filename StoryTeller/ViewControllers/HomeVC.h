//
//  HomeVC.h
//  StoryTeller
//
//  Created by nectarbits on 4/27/19.
//  Copyright © 2019 nectarbits. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface HomeVC : UIViewController 

@property (strong, nonatomic) IBOutlet UIButton *btnCreate;

@end

NS_ASSUME_NONNULL_END
