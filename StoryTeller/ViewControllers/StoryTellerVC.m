//
//  StoryTellerVCViewController.m
//  StoryTeller
//
//  Created by harshesh on 27/04/19.
//  Copyright © 2019 nectarbits. All rights reserved.
//

#import "StoryTellerVC.h"
#import "StoryTellerCell.h"
#import "SelectFrameViewTool.h"
#import "ColorView.h"
#import "MenuView.h"
#import "FontStyleClass.h"
#import "CLCircleView.h"
#import <Photos/Photos.h>
#import "Constant.h"
#import "AppUtility.h"


@interface CustomView : UIView <UIScrollViewDelegate>
-(id)initWithFrame:(CGRect)frame;
-(void)setimage:(UIImage*)image;
+ (void)setActiveScrollView:(CustomView*)view;

@end

@interface _CLTextViewEdit : UIView
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) NSString *FontName;
@property (nonatomic, assign) CGFloat fontSize;
@property (nonatomic, assign) CGFloat angle;
@property (nonatomic, strong) UIColor *fillColor;
@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic, assign) CGFloat borderWidth;
@property (nonatomic, assign) CGSize shadowOffset;
@property (nonatomic, strong) UIColor *shadowColor;
@property (nonatomic, assign) NSTextAlignment textAlignment;
+ (void)setActiveTextView:(_CLTextViewEdit*)view;
- (id)initWithTool:(CGRect)textFrame;
- (void)setScale:(CGFloat)scale;
- (void)sizeToFitWithMaxWidth:(CGFloat)width lineHeight:(CGFloat)lineHeight;
-(void)setLineSpace:(CGFloat)space wordSpace:(CGFloat)Wspace withMaxwidth:(CGFloat)maxWidth;
-(void)setBlurShadow:(CGFloat)BlurRadius;
-(void)setTextStrock:(CGFloat)strock StrockColor:(UIColor *)color;
- (void)setFont:(UIFont *)font withBoundWidth:(CGFloat)boundWidth;
-(void)setAlpha:(CGFloat)alpha;
-(void)setAngle:(CGFloat)angle;

@end

@interface StoryTellerVC () <SelectFrameViewToolDelegate,ColorViewDelegate,MenuClassDelegate,FontStyleClassDelegate,UITextViewDelegate>
{
    UIImage *StoreImage;
    NSMutableArray *ImageFrameArray;
    int count;
    int pageNumber;
//    NSArray *FrameArray;
    
    SelectFrameViewTool *SelectFrameView;
    ColorView *colorTool;
    MenuView *menuTool;
    FontStyleClass *fontTool;
    
    //Frame Array get frame from txt file
    NSMutableArray *FrameArray;
    //Frame Index
    NSInteger frameIndex;
    
    //Font Style Declaration
//    FontStyleClass *fontView;
    NSString *strFontStyle;
    float fontSize;
    UIImageView *_imageView;
    NSString *strText;
    BOOL isTextView;
    
    UIButton *btnDoneTextEdit;
    
//    NSMutableArray *arr;
    UIImage *finalImage;
    
    //For Preview
    UIImageView *PreviewImage;
    
    
}
@property (nonatomic, strong) CustomView *selectedCustomView;
@property (nonatomic, strong) _CLTextViewEdit *selectedTextView;
@end

@implementation StoryTellerVC
@synthesize TopHeightConstraint,lblNoStory,viewShare,btnPage,btnStory,btnShareToInstagram,btnSaveCancel,documentController,viewShareCard,viewDelete,viewDeleteCard,btnDeleteClose,btnDeleteCancel,btnDelete;

- (void)viewWillAppear:(BOOL)animated
{
    [self setUp];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    count = 1;
    ImageFrameArray = [[NSMutableArray alloc] init];
    
//    [ImageArray addObject:@"1"];
    StoreImage = [UIImage imageNamed: @"ic_placeholder"];//"ic_placeholder";
    // Do any additional setup after loading the view.

    pageNumber = 0;
    [self doInitialize];
    
    _clevStoryTeller.backgroundView = [[UIView alloc] initWithFrame:_clevStoryTeller.bounds];
    
    [_clevStoryTeller.backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchCollectionView:)]];
 
    if(ImageFrameArray.count != 0)
    {
        [_clevStoryTeller setHidden:NO];
        [lblNoStory setHidden:YES];
    }
    else{
        [_clevStoryTeller setHidden:YES];
        [lblNoStory setHidden:NO];
    }
    
//    [_clevStoryTeller reloadData];
    
    self.btnDeleteFrame.transform = CGAffineTransformMakeRotation(M_PI_2/2);
    
}

-(void)setUp
{
    //for sharing view
    viewShare.frame = CGRectMake(0, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:viewShare];
    [self.view bringSubviewToFront:viewShare];
    [viewShare setHidden:YES];
    viewShare.layer.shadowRadius  = 4.0f;
    viewShare.layer.shadowColor   = [UIColor grayColor].CGColor;
    viewShare.layer.shadowOffset  = CGSizeZero;
    viewShare.layer.shadowOpacity = 0.5f;
    
    viewShareCard.layer.cornerRadius = 20;
    viewShareCard.layer.masksToBounds = NO;
    btnPage.layer.cornerRadius = 15;
    btnStory.layer.cornerRadius = 15;
    btnShareToInstagram.layer.cornerRadius = 15;
    btnShareToInstagram.layer.borderWidth = 1;
    btnShareToInstagram.layer.borderColor = UIColor.blackColor.CGColor;
    
    //for delete view
    viewDelete.frame = CGRectMake(0, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:viewDelete];
    [self.view bringSubviewToFront:viewDelete];
    [viewDelete setHidden:YES];
    viewDelete.layer.shadowRadius  = 4.0f;
    viewDelete.layer.shadowColor   = [UIColor grayColor].CGColor;
    viewDelete.layer.shadowOffset  = CGSizeZero;
    viewDelete.layer.shadowOpacity = 0.5f;
    viewDeleteCard.layer.cornerRadius = 20;
    viewDeleteCard.layer.masksToBounds = NO;
    btnDelete.layer.cornerRadius = 15;
    btnDeleteCancel.layer.cornerRadius = 15;
    btnDeleteCancel.layer.borderWidth = 1;
    btnDeleteCancel.layer.borderColor = UIColor.blackColor.CGColor;
    
    //For Preview Image
    PreviewImage = [[UIImageView alloc] init];
    PreviewImage.frame = CGRectMake(0, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    [PreviewImage setUserInteractionEnabled:YES];
    [PreviewImage setBackgroundColor:[UIColor blackColor]];
    PreviewImage.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:PreviewImage];
    [self.view bringSubviewToFront:PreviewImage];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    
    // Setting the swipe direction.
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
    
    // Adding the swipe gesture on image view
    [PreviewImage addGestureRecognizer:swipeLeft];
    [PreviewImage addGestureRecognizer:swipeRight];
    [PreviewImage addGestureRecognizer:swipeDown];
    
    [PreviewImage setHidden:YES];
    
    strFontStyle = @"Digitalt";
    fontSize = 20;
    
    _btnAddFrame.layer.cornerRadius = 30;
    _btnDeleteFrame.layer.cornerRadius = 30;
    
    _txtEditView.delegate =  self;
    [_viewEdit setHidden:YES];
    
    _txtEditView.font = [UIFont fontWithName:@"Digitalt" size:30.0];
    
    menuTool = [[MenuView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height + 80, self.view.frame.size.width, 80)];
    menuTool.delegate = self;
    [self.view addSubview:menuTool];
    [self.view bringSubviewToFront:menuTool];
    [menuTool setHidden:YES];
    
    SelectFrameView = [[SelectFrameViewTool alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height + 200, self.view.frame.size.width, 200)];
    SelectFrameView.delegate = self;
    [self.view addSubview:SelectFrameView];
    [self.view bringSubviewToFront:SelectFrameView];
    [SelectFrameView setHidden:YES];
    
    colorTool = [[ColorView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height + 150, self.view.frame.size.width, 150)];
    colorTool.delegate = self;
    [self.view addSubview:colorTool];
    [menuTool bringSubviewToFront:colorTool];
    [colorTool setHidden:YES];
    
    fontTool = [[FontStyleClass alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height + 200, self.view.frame.size.width, 200)];
    fontTool.delegate =  self;
    [self.view addSubview: fontTool];
    [fontTool setHidden:YES];
    
}

-(void)doInitialize
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectScrollAction:) name:@"SelectScroll" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ChangeSelectedImage:) name:@"ChangeImage" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activeTextViewDidTap:) name:@"CLTextViewActiveViewDidTapNotificationString" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activeTextViewDidChange:) name:@"CLTextViewActiveViewDidChangeNotificationStringEdit" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteTextAction:) name:@"deleteText" object:nil];
    
}

//MARK:- Load Frame
-(void)addScrollsWithIndex:(NSInteger)index withFrame:(NSMutableArray*)frameArr  withText:(NSMutableArray*)textArr withCell:(StoryTellerCell *)clickedCell{
    
    if(index == pageNumber)
    {
        for (int j=0; j<frameArr.count; j++) {
            
            NSDictionary *d1 = [frameArr objectAtIndex:j];
            CGRect frame;
            float x;
            if ([[d1 objectForKey:@"x"] floatValue] == 0)
            {
                x = 0;
            }
            else
            {
                x = clickedCell.frame.origin.x + clickedCell.frame.size.width/[[NSString stringWithFormat:@"%@",[d1 objectForKey:@"x"]] floatValue];
            }
            
            float y;
            if ([[d1 objectForKey:@"y"] floatValue] == 0)
            {
                y = 0;
            }
            else
            {
                y = clickedCell.frame.origin.y + (clickedCell.frame.size.height/[[NSString stringWithFormat:@"%@",[d1 objectForKey:@"y"]] floatValue]);
            }
            
            float width = clickedCell.frame.size.width/[[NSString stringWithFormat:@"%@",[d1 objectForKey:@"width"]] floatValue];
            float height = clickedCell.frame.size.height/[[NSString stringWithFormat:@"%@",[d1 objectForKey:@"height"]] floatValue];
            float XPedding = [[NSString stringWithFormat:@"%@",[d1 objectForKey:@"XPedding"]] floatValue];
            float YPedding = [[NSString stringWithFormat:@"%@",[d1 objectForKey:@"YPedding"]] floatValue];
            float WPedding = [[NSString stringWithFormat:@"%@",[d1 objectForKey:@"WPedding"]] floatValue];
            float HPedding = [[NSString stringWithFormat:@"%@",[d1 objectForKey:@"HPedding"]] floatValue];
            
            frame = CGRectMake(x + XPedding, y + YPedding, width - WPedding, height - HPedding);
            
            CustomView *view = [[CustomView alloc] initWithFrame:frame];
            [view setBackgroundColor:[UIColor clearColor]];
            [view setClipsToBounds:YES];
            [clickedCell.contentView addSubview:view];
            [clickedCell.contentView bringSubviewToFront:view];
            [CustomView setActiveScrollView:view];
            [self ScrollSetAsdeselect];
            
            
        }
        
        if (textArr != nil){
            for (int j=0; j<textArr.count; j++) {
                
                NSDictionary *d1 = [textArr objectAtIndex:j];
                CGRect frame;
                float x;
                if ([[d1 objectForKey:@"x_position"] floatValue] == 0)
                {
                    x = 0;
                }
                else
                {
                    x = clickedCell.frame.origin.x + clickedCell.frame.size.width/[[NSString stringWithFormat:@"%@",[d1 objectForKey:@"x_position"]] floatValue];
                }
                
                float y;
                if ([[d1 objectForKey:@"y_position"] floatValue] == 0)
                {
                    y = 0;
                }
                else
                {
                    y = clickedCell.frame.origin.y + (clickedCell.frame.size.height/[[NSString stringWithFormat:@"%@",[d1 objectForKey:@"y_position"]] floatValue]);
                }
                
                float width = clickedCell.frame.size.width/[[NSString stringWithFormat:@"%@",[d1 objectForKey:@"text_width"]] floatValue];
                float height = clickedCell.frame.size.height/[[NSString stringWithFormat:@"%@",[d1 objectForKey:@"text_height"]] floatValue];
                frame = CGRectMake(x, y, width, height);
                
                NSString *text = [NSString stringWithFormat:@"%@", [d1 objectForKey: @"text"]];
                CGFloat fontSize = [[d1 objectForKey:@"size"] floatValue];
                _CLTextViewEdit *view = [[_CLTextViewEdit alloc] initWithTool:frame];
                
                CGFloat ratio = MIN( (0.8 * _clevStoryTeller.frame.size.width) / view.frame.size.width, (0.2 * _clevStoryTeller.frame.size.height) / view.frame.size.height);
                [view setScale:ratio];
                view.tag = 2003;
                [clickedCell.contentView addSubview:view];
                [clickedCell.contentView bringSubviewToFront:view];
                [_CLTextViewEdit setActiveTextView:view];
                [fontTool setFontCategory];
                [view setText:text];
                [view setFont:[UIFont fontWithName:@"Digitalt" size: fontSize] withBoundWidth:clickedCell.frame.size.width - 70];
                [_CLTextViewEdit setActiveTextView:nil];
            }
        }
    }
    
}

-(void) ScrollSetAsdeselect{
    
    [CustomView setActiveScrollView:nil];
    
}

//MARK:- Image Picker Delegate
-(void)Selectimage{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    //    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    StoreImage = chosenImage;
    [_selectedCustomView setimage:StoreImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

//MARK:- Collection delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return ImageFrameArray.count;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"StoryTellerCell";
    
    StoryTellerCell *cell = (StoryTellerCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [self addScrollsWithIndex:indexPath.row withFrame:ImageFrameArray[indexPath.row][@"frame"] withText:ImageFrameArray[indexPath.row][@"text"] withCell:cell];
    
    cell.layer.shadowRadius  = 4.0f;
    cell.layer.shadowColor   = [UIColor grayColor].CGColor;
    cell.layer.shadowOffset  = CGSizeZero;
    cell.layer.shadowOpacity = 1.2f;
//        cell.layer.masksToBounds = NO;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(_clevStoryTeller.frame.size.width - 70,_clevStoryTeller.frame.size.height - 50);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{

    return UIEdgeInsetsMake(25, 35, 40, 35);//top Left bottom right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 70;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //For get indexpath of CollectionView
//    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:pageNumber inSection:0];
//    StoryTellerCell *clickedCell = (StoryTellerCell *)[_clevStoryTeller cellForItemAtIndexPath:indexPath];
    
    //For get indexpath of CollectionView
    CGFloat pageWidth =_clevStoryTeller.frame.size.width;
    float currentPage = _clevStoryTeller.contentOffset.x/ pageWidth;
    
    float page = ceil(currentPage);
    
    if (0.0f != fmodf(currentPage, 1.0f))
    {
        
        page = currentPage + 1;
    }
    else
    {
        page = currentPage;
    }

    pageNumber = ceil(currentPage);
    NSLog(@"Page Number : %ld", (long)ceil(currentPage));
 
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    NSLog(@"End Scrolling Animation call");
    
    for (StoryTellerCell *cell in [self.clevStoryTeller visibleCells]) {
        NSIndexPath *indexPath = [self.clevStoryTeller indexPathForCell:cell];
        NSLog(@"%@",indexPath);
        
            UIGraphicsBeginImageContextWithOptions(cell.bounds.size, cell.opaque, 0.0);
            [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage * screenshot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            // Render the screen shot at custom resolution /
            //        CGRect cropRect = CGRectMake(0 ,0 ,1242 ,2208);
            CGRect cropRect = CGRectMake(0 ,0 ,1080 ,1920);
            UIGraphicsBeginImageContextWithOptions(cropRect.size, cell.opaque, 1.0f);
            [screenshot drawInRect:cropRect];
            UIImage * customScreenShot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            if(screenshot!=nil)
            {
                NSLog(@"Sucess image get from End Scrolling");
                if(!PreviewImage.isHidden)
                {
                    PreviewImage.image = nil;
                    PreviewImage.image = customScreenShot;
                }
            }
            else
            {
                NSLog(@"Not");
                PreviewImage.image = customScreenShot;
            }
    }
}

- (void)touchCollectionView:(UITapGestureRecognizer*)sender
{
    [self ScrollSetAsdeselect];
    [self textSetAsdeselect];
    [_CLTextViewEdit setActiveTextView:nil];
    
    if(!menuTool.isHidden)
    {
        [self hideMenuView];
    }
    if(!fontTool.isHidden)
    {
        [self hideFontStyleTool];
    }
    if(!colorTool.isHidden)
    {
        [self hideColorViewTool];
    }
    if(!SelectFrameView.isHidden)
    {
        [self hideSelectedFrameView];
    }
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"Left Swipe");
        
        if(pageNumber < (ImageFrameArray.count - 1))
        {
            pageNumber = pageNumber + 1;
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:pageNumber inSection:0];
            [self.clevStoryTeller scrollToItemAtIndexPath:indexpath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
        else{
            NSLog(@"page number is bigger");
        }
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        NSLog(@"Right Swipe");
    
        if(pageNumber > 0)
        {
            pageNumber = pageNumber - 1;
            
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:pageNumber inSection:0];
            [self.clevStoryTeller scrollToItemAtIndexPath:indexpath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
        else
        {
            NSLog(@"Page NUmber is Small");
        }
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionDown) {
        NSLog(@"Down Swipe");
        
        [UIView animateWithDuration:0.3 animations:^{
            PreviewImage.frame = CGRectMake(0, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        } completion:^(BOOL finished) {
            if(finished)
            {
                [PreviewImage setHidden:YES];
            }
        }];
    }
}

//MARK:- Menu View Delegate
-(void)openMenuView
{
    [self ScrollSetAsdeselect];
    
    [menuTool setHidden:NO];
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         menuTool.frame = CGRectMake(0, menuTool.frame.origin.y - 160, menuTool.frame.size.width, menuTool.frame.size.height);
                         
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)hideMenuView
{
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         menuTool.frame = CGRectMake(0, menuTool.frame.origin.y + 160, menuTool.frame.size.width, menuTool.frame.size.height);
                         
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         [menuTool setHidden:YES];
                     }];
}

-(void)hideMenuTool
{
    [self hideMenuView];
}

-(void)SelectedIndexMenu:(NSInteger)index
{
    if(index == 0)
    {
        [self openColorView];
    }
    else if(index == 1)
    {
        [self addNewText];
//        [self openFontStyleTool];
        NSLog(@"Selected index : %ld",(long)index);
    }
}

//MARK:- SelectFrame Delegate
-(void)openSelectFrameView
{
    [SelectFrameView setBackgroundCategory];
    [SelectFrameView setHidden:NO];
    [UIView animateWithDuration:0.3
                     animations:^{
                    
                                 SelectFrameView.frame = CGRectMake(0, SelectFrameView.frame.origin.y - 400, SelectFrameView.frame.size.width, SelectFrameView.frame.size.height);
                         
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)hideSelectedFrameView
{
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         SelectFrameView.frame = CGRectMake(0, SelectFrameView.frame.origin.y + 400, SelectFrameView.frame.size.width, SelectFrameView.frame.size.height);
                         
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         [SelectFrameView setHidden:YES];
                     }];
}

-(void)selectedFrame:(NSMutableArray *)frameArr text:(nonnull NSMutableArray *)textArray{
    NSLog(@"%ld",(long)index);
    
    NSLog(@"%@",frameArr);
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    dict[@"text"] = textArray;
    dict[@"frame"] = frameArr;
    [ImageFrameArray addObject:dict];
    NSLog(@"%@", ImageFrameArray);
    
    pageNumber = (int)ImageFrameArray.count - 1;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:pageNumber inSection:0];

    [_clevStoryTeller insertItemsAtIndexPaths:@[indexPath]];
    
    if(ImageFrameArray.count != 0)
    {
        [_clevStoryTeller setHidden:NO];
        [lblNoStory setHidden:YES];
    }
    
    [self.clevStoryTeller scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    [self hideSelectedFrameView];
    
    
}

//MARK:- ColorTool Delegate
-(void)openColorView
{
    [colorTool setBackgroundColorCategory];
    [colorTool setHidden:NO];
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         colorTool.frame = CGRectMake(0, colorTool.frame.origin.y - 300, colorTool.frame.size.width, colorTool.frame.size.height);
                         
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)hideColorViewTool
{
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         colorTool.frame = CGRectMake(0, colorTool.frame.origin.y + 300, colorTool.frame.size.width, colorTool.frame.size.height);
                         
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         if(finished)
                         {
                             [colorTool setHidden:YES];
                         }
                         
                     }];
}

-(void)SelectedColor:(UIColor *)color
{
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:pageNumber inSection:0];
//    StoryTellerCell *clickedCell = (StoryTellerCell *)[_clevStoryTeller cellForItemAtIndexPath:indexPath];
    
    for (UICollectionViewCell *cell in [self.clevStoryTeller visibleCells]) {
        NSIndexPath *indexPath = [self.clevStoryTeller indexPathForCell:cell];
        NSLog(@"%@",indexPath);
        
        cell.contentView.backgroundColor = color;
        
    }
    
//    clickedCell.contentView.backgroundColor = color;
}

-(void)hidecolorView
{
    [self hideColorViewTool];
}

//MARK:- Font Tool Delegate
-(void)openFontStyleTool
{
    
    [fontTool setFontCategory];
    [fontTool setHidden:NO];
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         fontTool.frame = CGRectMake(0, fontTool.frame.origin.y - 400, fontTool.frame.size.width, fontTool.frame.size.height);
                         
//                         TopHeightConstraint.constant = 0;
                         
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}

-(void)hideFontStyleTool
{
    [_CLTextViewEdit setActiveTextView:nil];
    [self textSetAsdeselect];
    [self.view updateConstraintsIfNeeded];
    [fontTool setFontCategory];
    [fontTool setHidden:YES];
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                                 fontTool.frame = CGRectMake(0, fontTool.frame.origin.y + 400, fontTool.frame.size.width, fontTool.frame.size.height);
                         
//                         self.TopHeightConstraint.constant = 50;
                         
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         
                }];
}

-(void) textSetAsdeselect{
    //Art selection remove
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:pageNumber inSection:0];
    StoryTellerCell *clickedCell = (StoryTellerCell *)[_clevStoryTeller cellForItemAtIndexPath:indexPath];
    
    
    for (id i in clickedCell.subviews){
        
        if( [i isKindOfClass:[UIView class]]){
            UIView *viewImage = (UIView *)i;
            if (viewImage.tag != 2001)
            {
                if (viewImage.tag == 2003)
                {
                    [_CLTextViewEdit setActiveTextView:nil];
                }
            }
            
        }
    }
}

- (void)deleteTextAction:(NSNotification*)notification
{
   
    [self textSetAsdeselect];
    
    if (!fontTool.isHidden){
        [self hideFontStyleTool];
    }
}

- (void)activeTextViewDidChange:(NSNotification*)notification
{
    self.selectedTextView = notification.object;
    
    if(fontTool.isHidden)
    {
        [self openFontStyleTool];
    }
    
    if(!menuTool.isHidden)
    {
        [self hideMenuView];
    }
    
}

-(void)selectedColor:(UIColor *)color {
    if (@available(iOS 11.0, *)) {
        [_selectedTextView setFillColor:color];
    } else {
        // Fallback on earlier versions
        [_selectedTextView setFillColor:color];
        
    }
}

- (void)activeTextViewDidTap:(NSNotification*)notification
{
    self.selectedTextView = notification.object;
    
    [_viewEdit setHidden:false];
    _viewEdit.transform = CGAffineTransformMakeScale(1.3, 1.3);
    [UIView animateWithDuration:0.3
                     animations:^{
                         _viewEdit.transform = CGAffineTransformMakeScale(1.0, 1.0);

                     }];
    _txtEditView.text = self.selectedTextView.text;
    [_txtEditView setFont:[_txtEditView.font fontWithSize: 30]];
    _txtEditView.inputAccessoryView = nil;
    _txtEditView.autocorrectionType = UITextAutocorrectionTypeYes;
    [_txtEditView becomeFirstResponder];
}

- (void)setSelectedTextView:(_CLTextViewEdit *)selectedTextView
{
    _selectedTextView = selectedTextView;
}

-(void)selectedShadowColor:(UIColor *)colorShadow
{
    [_selectedTextView setShadowColor:colorShadow];
}

-(void)selectedFontStyle:(NSString *)strFont {
    strFontStyle = strFont;
    
    [_selectedTextView setFont:[UIFont fontWithName:strFontStyle size:_selectedTextView.fontSize] withBoundWidth:_clevStoryTeller.frame.size.width - 70];
}

-(void)selectedFontSize:(float )selectedFontSize {
    fontSize = selectedFontSize;
    [_selectedTextView setFont:[UIFont fontWithName:strFontStyle size:fontSize] withBoundWidth:_clevStoryTeller.frame.size.width - 70];
    [[NSUserDefaults standardUserDefaults] setFloat:fontSize forKey:@"fontsize"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)selectedFontShadow:(float )fontShadow Type:(int)Angle{
    
    if(Angle == 1)
    {
        [_selectedTextView setShadowOffset:CGSizeMake(fontShadow, 0)];
    }
    else if(Angle == 2)
    {
        [_selectedTextView setShadowOffset:CGSizeMake(0, fontShadow)];
    }
    else if(Angle == 3)
    {
        [_selectedTextView setShadowOffset:CGSizeMake(-fontShadow, 0)];
    }
    else
    {
        [_selectedTextView setShadowOffset:CGSizeMake(0, -fontShadow)];
    }
    
}

-(void)addNewText{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:pageNumber inSection:0];
    StoryTellerCell *clickedCell = (StoryTellerCell *)[_clevStoryTeller cellForItemAtIndexPath:indexPath];
    
    _CLTextViewEdit *view = [[_CLTextViewEdit alloc] initWithTool:CGRectMake(0, 100, 132, 132)];
    
    CGFloat ratio = MIN( (0.8 * _clevStoryTeller.frame.size.width) / view.frame.size.width, (0.2 * _clevStoryTeller.frame.size.height) / view.frame.size.height);
    [view setScale:ratio];
    //view.center = CGPointMake(_collageFrame.frame.size.width/2, view.frame.size.height/2 + 30);
    view.center = CGPointMake((_clevStoryTeller.frame.size.width/2) - 35, (_clevStoryTeller.frame.size.height/2) - 25);
    view.tag = 2003;
    [clickedCell addSubview:view];
    [clickedCell bringSubviewToFront:view];
    [_CLTextViewEdit setActiveTextView:view];
    [fontTool setFontCategory];
    [view setText:@"Begin writing your story here."];
    [view setFont:[UIFont fontWithName:@"Digitalt" size:20] withBoundWidth:clickedCell.frame.size.width - 70];
    
    [self textSetAsdeselect];
    
}

-(void)setLineSpacing:(float)spacing withWordSpacing:(float)wordSpacing
{
    [_selectedTextView setLineSpace:spacing wordSpace:wordSpacing withMaxwidth:_clevStoryTeller.frame.size.width];
    //    [_selectedTextView setWordSpace:wordSpacing];
}

-(void)setWordSpacing:(float)Wordspacing withLineSpeacing:(float)lineSpacing
{
    //    [_selectedTextView setWordSpace:Wordspacing];
    [_selectedTextView setLineSpace:lineSpacing wordSpace:Wordspacing withMaxwidth:_clevStoryTeller.frame.size.width];
    
}

-(void)selectedBlurShadow:(float)BlurRadius
{
    [_selectedTextView setBlurShadow:BlurRadius];
}

-(void)selectedTextAlignment:(NSTextAlignment)Alignment
{
    [_selectedTextView setTextAlignment:Alignment];
}

-(void)setTextAlpha:(float)alpha
{
    [_selectedTextView setAlpha:alpha];
}

-(void)setTextAngle:(float)angle
{
    [_selectedTextView setAngle:angle];
}


-(void)selectedStrockSize:(float)Strock StrockColor:(UIColor *)color
{
    [_selectedTextView setTextStrock:Strock StrockColor:color];
}

-(void)selectedRotationX:(float)txtRotationX
{
    float val = txtRotationX;
    
    CATransform3D TxtTransformX = CATransform3DIdentity;
    TxtTransformX.m34 = -1.0 / 500;
    TxtTransformX = CATransform3DRotate(TxtTransformX, val * M_PI / 115.0, 1.0f, 0.0f, 0.0f);
    _selectedTextView.layer.zPosition = CGRectGetWidth(_selectedTextView.bounds);
    _selectedTextView.layer.transform = TxtTransformX;
    
}

-(void)selectedRotationY:(float)txtRotationY
{
    float val = txtRotationY;
    
    CATransform3D TxtTransformY = CATransform3DIdentity;
    TxtTransformY.m34 = -1.0 / 500;
    TxtTransformY = CATransform3DRotate(TxtTransformY, val * M_PI / 115.0, 0.0f, 1.0f, 0.0f);
    _selectedTextView.layer.zPosition = CGRectGetWidth(_selectedTextView.bounds);
    _selectedTextView.layer.transform = TxtTransformY;
}

//MARK:- Notification Methods
- (void)selectScrollAction:(NSNotification*)notification
{
    self.selectedCustomView = notification.object;

    [CustomView setActiveScrollView:_selectedCustomView];
    
//    [self.selectedCustomView.layer setBorderWidth:2.0];
//    [self.selectedCustomView.layer setBorderColor:[UIColor whiteColor].CGColor];
}

- (void)ChangeSelectedImage:(NSNotification*)notification
{
    self.selectedCustomView = notification.object;
    
    [self Selectimage];
}

//MARK:- Download & save image To Gallary
-(void)ConvertImageResolution:(NSString*)strResolution
{
    
    for (UICollectionViewCell *cell in [self.clevStoryTeller visibleCells]) {
        NSIndexPath *indexPath = [self.clevStoryTeller indexPathForCell:cell];
        NSLog(@"%@",indexPath);
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
//            UIGraphicsBeginImageContextWithOptions(cell.bounds.size, cell.opaque, 0.0f);
//            [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
//            [cell drawViewHierarchyInRect:cell.bounds afterScreenUpdates:YES];
//            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//            //    image = [image imageByTrimmingTransparentPixelsRequiringFullOpacity:YES];
//            UIGraphicsEndImageContext();
            
            UIGraphicsBeginImageContextWithOptions(cell.bounds.size, cell.opaque, 0.0);
            [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage * screenshot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            // Render the screen shot at custom resolution /
//            CGRect cropRect = CGRectMake(0 ,0 ,1242 ,2208);
            CGRect cropRect = CGRectMake(0 ,0 ,1080 ,1920);
            UIGraphicsBeginImageContextWithOptions(cropRect.size, cell.opaque, 1.0f);
            [screenshot drawInRect:cropRect];
            UIImage * customScreenShot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            [self saveImageToGallary:customScreenShot];
        });
    }
}

-(void)saveImageToGallary:(UIImage*)_finalImage
{
    [AppUtility show:self.view];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
                
                PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
                
                if (status == PHAuthorizationStatusAuthorized)
                {
                    //We have permission. Do whatever is needed
                    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^
                     {
                         PHAssetChangeRequest *changeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:_finalImage];
                         
                         changeRequest.creationDate          = [NSDate date];
                     }
                                                      completionHandler:^(BOOL success, NSError *error)
                     {
                         if (success)
                         {
                             [AppUtility hide:self.view];
                             NSLog(@"successfully saved");
                             
                             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your photo save successfully" preferredStyle:UIAlertControllerStyleAlert];
                             [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                 
                             }]];
                             [self presentViewController:alert animated:YES completion:nil];
                             
                         }
                         else
                         {
                             [AppUtility hide:self.view];
                             NSLog(@"error saving to photos: %@", error);
                             
                             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error!" message:error.debugDescription preferredStyle:UIAlertControllerStyleAlert];
                             [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                 
                             }]];
                             [self presentViewController:alert animated:YES completion:nil];
                             
                         }
                     }];
                }
                else
                {
                    [AppUtility hide:self.view];
                    //No permission. Trying to normally request it
                    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                        
                        if (status != PHAuthorizationStatusAuthorized)
                        {
                            NSString *accessDescription = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSPhotoLibraryUsageDescription"];
                            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:accessDescription message:@"To give permissions tap on 'Change Settings' button" preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                            [alertController addAction:cancelAction];
                            
                            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Change Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                            }];
                            [alertController addAction:settingsAction];
                            
                            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
                        }
                    }];
                }
                
                
            }
            
            else {
                [AppUtility hide:self.view];
                // Access has been denied.
                NSString *accessDescription = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSPhotoLibraryUsageDescription"];
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:accessDescription message:@"To give permissions tap on 'Change Settings' button" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:cancelAction];
                
                UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Change Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }];
                [alertController addAction:settingsAction];
                
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
            }
        }];
    });
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.viewShare setFrame:CGRectMake(0.0f, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        
    } completion:^(BOOL completed){
        if(completed) {
            [self.viewShare setHidden:YES];
        }
    }];
    
}

//MARK:- Share with Instagram
-(void)savePostsPhotoBeforeSharing {
    
    for (UICollectionViewCell *cell in [self.clevStoryTeller visibleCells]) {
        NSIndexPath *indexPath = [self.clevStoryTeller indexPathForCell:cell];
        NSLog(@"%@",indexPath);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            finalImage = nil;
//            UIGraphicsBeginImageContextWithOptions(cell.bounds.size, cell.opaque, 0.0f);
//            [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
//            [cell drawViewHierarchyInRect:cell.bounds afterScreenUpdates:YES];
//            finalImage = UIGraphicsGetImageFromCurrentImageContext();
//            UIGraphicsEndImageContext();
            
            UIGraphicsBeginImageContextWithOptions(cell.bounds.size, cell.opaque, 0.0);
            [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage * screenshot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            // Render the screen shot at custom resolution /
//            CGRect cropRect = CGRectMake(0 ,0 ,1242 ,2208);
            CGRect cropRect = CGRectMake(0 ,0 ,1080 ,1920);
            UIGraphicsBeginImageContextWithOptions(cropRect.size, cell.opaque, 1.0f);
            [screenshot drawInRect:cropRect];
            finalImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            UIImageWriteToSavedPhotosAlbum(finalImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL); //Choosen image is image that you need to send on Instagram
        });
    }
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo: (void *) contextInfo; {
    [self instaGramWallPost]; // this function save image .io format and check if any error exist or not
}

-(void)instaGramWallPost
{
    [AppUtility show:self.view];
//    UIImage *imgShare = _defaultSharingImage;
    PHFetchOptions *fetchOptions = [PHFetchOptions new];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO],];
    __block PHAsset *assetToShare;
    PHFetchResult *result = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
    [result enumerateObjectsUsingBlock:^(PHAsset *asset, NSUInteger idx, BOOL *stop) {
        assetToShare = asset;
    }];
    if([assetToShare isKindOfClass:[PHAsset class]])
    {
        NSString *localIdentifier = assetToShare.localIdentifier;
        NSString *urlString = [NSString stringWithFormat:@"instagram://library?LocalIdentifier=1% @",localIdentifier];
        
        NSURL *instagramURL = [NSURL URLWithString:urlString];
        if ([[UIApplication sharedApplication] canOpenURL: instagramURL]) {
            [AppUtility hide:self.view];
            [[UIApplication sharedApplication] openURL: instagramURL];
            
        } else {
            
            [AppUtility hide:self.view];
            UIAlertController *removedSuccessFullyAlert = [UIAlertController alertControllerWithTitle:@"Error!" message:@"No Instagram Application Found!" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
            [removedSuccessFullyAlert addAction:firstAction];
            [self presentViewController:removedSuccessFullyAlert animated:YES completion:nil];
        }
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.viewShare setFrame:CGRectMake(0.0f, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        
    } completion:^(BOOL completed){
        if(completed) {
            [self.viewShare setHidden:YES];
        }
    }];
    
}


- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

- (void)documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller {
    
}
//MARK:- Action Methods
- (IBAction)btnBackTap:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDownloadTap:(id)sender {
    
    [self ScrollSetAsdeselect];
    [self textSetAsdeselect];
    if(!menuTool.isHidden)
    {
        [self hideMenuView];
    }
    
    if(!colorTool.isHidden)
    {
        [self hideColorViewTool];
    }
    
    if(!fontTool.isHidden)
    {
        [self hideFontStyleTool];
    }
    
    if(!SelectFrameView.isHidden)
    {
        [self hideSelectedFrameView];
    }
    
    //For Open save Menu
    if(ImageFrameArray.count != 0)
    {
        [viewShare setHidden:NO];
        [UIView animateWithDuration:0.3 animations:^{
            
            [self.viewShare setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
            
        } completion:nil];
    }
    
}

- (IBAction)btnMenuTap:(UIButton *)sender {
 
    if(ImageFrameArray.count != 0)
    {
        [self openMenuView];
    }
    
}

- (IBAction)btnShowTap:(UIButton *)sender {
 
    if(ImageFrameArray.count != 0)
    {
        [CustomView setActiveScrollView:nil];
        
        [PreviewImage setHidden:NO];
        PreviewImage.frame = CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height - 20);
        
        for (UICollectionViewCell *cell in [self.clevStoryTeller visibleCells]) {
            NSIndexPath *indexPath = [self.clevStoryTeller indexPathForCell:cell];
            NSLog(@"%@",indexPath);
            
            //        UIGraphicsBeginImageContext(cell.bounds.size);
            //        CGContextRef ctx = UIGraphicsGetCurrentContext();
            //        [cell drawViewHierarchyInRect:cell.bounds afterScreenUpdates:NO];
            //        UIImage* img = UIGraphicsGetImageFromCurrentImageContext();
            
            UIGraphicsBeginImageContextWithOptions(cell.bounds.size, cell.opaque, 0.0);
            [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage * screenshot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
//            CGRect cropRect = CGRectMake(0 ,0 ,1242 ,2208);
            CGRect cropRect = CGRectMake(0 ,0 ,1080 ,1920);
            UIGraphicsBeginImageContextWithOptions(cropRect.size, YES, 1.0f);
            [screenshot drawInRect:cropRect];
            UIImage * customScreenShot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            UIGraphicsEndImageContext();
            
            if(screenshot!=nil)
            {
                NSLog(@"Sucess image get");
                PreviewImage.image = customScreenShot;
            }
            else
            {
                NSLog(@"Not");
            }
            
        }
    }

}

- (IBAction)btnAddFrameTap:(UIButton *)sender {
    
    [self openSelectFrameView];
    
}

-(IBAction)btnDeleteFrameTap:(id)sender{
    
    if(ImageFrameArray.count != 0)
    {
        [viewDelete setHidden:NO];
        [UIView animateWithDuration:0.3 animations:^{
            
            [self.viewDelete setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
            
        } completion:nil];
    }
    
}

- (IBAction)btnEditDoneAction:(id)sender {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:pageNumber inSection:0];
    StoryTellerCell *clickedCell = (StoryTellerCell *)[_clevStoryTeller cellForItemAtIndexPath:indexPath];
    
        [UIView animateWithDuration:0.3
                         animations:^{
                             _viewEdit.transform = CGAffineTransformMakeScale(1.3, 1.3);
                             
                             if ([strText isEqualToString:@""])
                             {
                                 if ([strText isEqual:@"initialText"]){
                                     
                                 }
                                 else
                                 {
                                     [self addNewText];
                                 }
                             }
                             
                         }completion:^(BOOL finished) {
                             if (finished)
                             {
                                 [_txtEditView resignFirstResponder];
                                 [_viewEdit setHidden:true];
                                 if (_txtEditView.text.length == 0) {
                                     strText = @"";
                                 }else {
                                     strText = _txtEditView.text;
                                 }
                                 //            self.selectedTextView.text = @"";
                                 //            self.selectedTextView.text = strText;
                                 [self.selectedTextView setText:strText];
                                 [self.selectedTextView setFont:[UIFont fontWithName:_selectedTextView.FontName size:_selectedTextView.fontSize] withBoundWidth:clickedCell.frame.size.width];
                                 CGFloat angle = _selectedTextView.angle;
                                 [self.selectedTextView setAngle:angle];
                             }
                         }];
    
}

- (IBAction)btnEditCancelAction:(id)sender {
    
        [UIView animateWithDuration:0.3
                         animations:^{
                             _viewEdit.transform = CGAffineTransformMakeScale(1.3, 1.3);
                             //            self.bgView.alpha = 0.0;
                         }completion:^(BOOL finished) {
                             if (finished)
                             {
                                 [_txtEditView resignFirstResponder];
                                 [_viewEdit setHidden:true];
                                 [self hideFontStyleTool];
                             }
                         }];
    
}

- (IBAction)btnPageTap:(id)sender {
    
    [self ConvertImageResolution:@"High"];
}

- (IBAction)btnStoryTap:(id)sender {
    
    int i = 1;
    
    
    
    if(i < (ImageFrameArray.count - 1))
    {
        
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        [self.clevStoryTeller scrollToItemAtIndexPath:indexpath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        i = i+1;
    }
    else
    {
        i = 0;
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        [self.clevStoryTeller scrollToItemAtIndexPath:indexpath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        
    }
    
}

- (IBAction)btnShareToInstagramTap:(id)sender {
    
    [self savePostsPhotoBeforeSharing];
}

- (IBAction)btnSaveCancelTap:(id)sender {

    [UIView animateWithDuration:0.3 animations:^{
        
        [self.viewShare setFrame:CGRectMake(0.0f, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        
    } completion:^(BOOL completed){
        if(completed) {
            [self.viewShare setHidden:YES];
        }
    }];
}

- (IBAction)btnDeleteTap:(id)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.viewDelete setFrame:CGRectMake(0.0f, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        
    } completion:^(BOOL completed){
        if(completed) {
            [self.viewDelete setHidden:YES];
            
            if(ImageFrameArray.count != 0)
            {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:pageNumber inSection:0];
                //        StoryTellerCell *clickedCell = (StoryTellerCell *)[_clevStoryTeller cellForItemAtIndexPath:indexPath];
                
                id item = ImageFrameArray[indexPath.row];
                NSMutableArray *updatedFeedItems = [ImageFrameArray mutableCopy];
                [updatedFeedItems removeObject:item];
                ImageFrameArray = [NSMutableArray arrayWithArray:updatedFeedItems];
                
                pageNumber = (int)ImageFrameArray.count - 1;
                
                // Now delete the items from the collection view.
                [self.clevStoryTeller deleteItemsAtIndexPaths:@[indexPath]];
                
                if(ImageFrameArray.count == 0)
                {
                    [_clevStoryTeller setHidden:YES];
                    [lblNoStory setHidden:NO];
                }
                else
                {
                    [_clevStoryTeller setHidden:NO];
                    [lblNoStory setHidden:YES];
                    
                    NSIndexPath *indexpath1 = [NSIndexPath indexPathForRow:ImageFrameArray.count - 1 inSection:0];
                    [self.clevStoryTeller scrollToItemAtIndexPath:indexpath1 atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
                }
            }
            else{
                NSLog(@"Frame Array is Empty");
            }
        }
    }];
    
    
}

- (IBAction)btnDeleteCancelTap:(id)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.viewDelete setFrame:CGRectMake(0.0f, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        
    } completion:^(BOOL completed){
        if(completed) {
            [self.viewDelete setHidden:YES];
        }
    }];
}

- (IBAction)btnDeleteCloseTap:(id)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.viewDelete setFrame:CGRectMake(0.0f, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        
    } completion:^(BOOL completed){
        if(completed) {
            [self.viewDelete setHidden:YES];
        }
    }];
}


- (BOOL)prefersStatusBarHidden
{
    return YES;
}
@end

@implementation CustomView{
    
    UIImageView *img;
    UIScrollView *scroll;
    CAShapeLayer *yourViewBorder;
    
    CGFloat _scale;

    
    CGPoint _initialPoint;
    CGFloat _initialArg;
    CGFloat _initialScale;
}

+ (void)setActiveScrollView:(CustomView*)view
{
    static CustomView *activeView = nil;
    if(view != activeView){
        [activeView setAvtive:NO];
        activeView = view;
        [activeView setAvtive:YES];
        
//        [activeView.superview bringSubviewToFront:activeView];
    }
}

- (void)setAvtive:(BOOL)active
{
    scroll.layer.borderWidth = (active) ? 1.5/_scale : 0;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        scroll.layer.borderColor = [UIColor blackColor].CGColor;
        scroll.layer.borderWidth = 3;
        scroll.clipsToBounds = YES;
        scroll.layer.masksToBounds = YES;
        scroll.delegate = self;
        scroll.layer.cornerRadius = 5;
        [self addSubview:scroll];
        
        img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        img.image = [UIImage imageNamed:@"ic_addImage"];
        img.contentMode = UIViewContentModeCenter;
        img.userInteractionEnabled = YES;
        img.backgroundColor = [UIColor colorWithRed:224.0/255.0 green:224.30/255.0 blue:224.0/255.0 alpha:1.0];
        img.clipsToBounds = YES;
        [scroll addSubview:img];
        _scale = 1;
        
        yourViewBorder = [CAShapeLayer layer];
        yourViewBorder.strokeColor = [UIColor blackColor].CGColor;
        yourViewBorder.fillColor = nil;
        yourViewBorder.lineDashPattern = @[@8, @2];
        yourViewBorder.frame = self.bounds;
        yourViewBorder.path = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
        [self.layer addSublayer:yourViewBorder];
        [self initGuesture];
    }
    return self;
}

-(void)initGuesture
{
    img.userInteractionEnabled = YES;
    [img addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTap:)]];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired = 2;
    [img addGestureRecognizer:tapGesture];
}

- (void)view:(UIView*)view setCenter:(CGPoint)centerPoint withScrollview:(UIScrollView*)scrollView
{
    //Image set center in imageview
    CGRect vf = view.frame;
    CGPoint co = scrollView.contentOffset;
    
    CGFloat x = centerPoint.x - vf.size.width / 2.0;
    CGFloat y = centerPoint.y - vf.size.height / 2.0;
    
    if(x < 0)
    {
        co.x = -x;
        vf.origin.x = 0.0;
    }
    else
    {
        vf.origin.x = x;
    }
    if(y < 0)
    {
        co.y = -y;
        vf.origin.y = 0.0;
    }
    else
    {
        vf.origin.y = y;
    }
    
    view.frame = vf;
    scrollView.contentOffset = co;
}

//MARK:- Scrollview Delegate Method
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    for (UIScrollView *y in scrollView.subviews){
        if( [y isKindOfClass:[UIImageView class]]){
            UIImageView *imgOne = (UIImageView*)y;
            CGPoint scrollPointInView = CGPointMake(imgOne.frame.origin.x, imgOne.frame.origin.y);
            CGRect frameRelativeToParent1 = [imgOne convertRect:imgOne.frame toView:scrollView];
            if (CGRectContainsPoint(frameRelativeToParent1 , scrollPointInView)){
                img = imgOne;
                return img;
            }
        }
    }
    return nil;
}

- (UIImage*)resizeImage:(UIImage*)aImage reSize:(CGSize)newSize;
{
    CGFloat widthFactor = aImage.size.width / newSize.width;
    CGFloat heightFactor = aImage.size.height / newSize.height;
    
    CGFloat resizeFactor;
    if (widthFactor > heightFactor) {
        resizeFactor = heightFactor;
    }else{
        resizeFactor = widthFactor;
    }
    
    CGSize aNewSize = CGSizeMake(aImage.size.width/resizeFactor, aImage.size.height/resizeFactor);
    
    UIGraphicsBeginImageContextWithOptions(aNewSize, YES, 0.0);
    [aImage drawInRect:CGRectMake(0,0,aNewSize.width,aNewSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)setimage:(UIImage *)image
{
    img.image = nil;
    img.contentMode = UIViewContentModeScaleAspectFill;
    UIImage *tempImage = image;
    UIImage *imgCompressed = [self resizeImage:tempImage reSize:scroll.frame.size];
    scroll.contentSize = CGSizeMake(imgCompressed.size.width, imgCompressed.size.height);
    CGRect frameimg = (CGRect){.origin=CGPointMake(0.0f, 0.0f), scroll.contentSize};
    img.frame = frameimg;
    img.image = imgCompressed;
    
    //Set Border Stork Line Hidden
    yourViewBorder.lineDashPattern = @[@0, @0];
    yourViewBorder.hidden = YES;
    CGFloat scaleWidth = frameimg.size.width / scroll.contentSize.width;
    CGFloat scaleHeight = frameimg.size.height / scroll.contentSize.height;
    CGFloat minScale = MIN(scaleWidth, scaleHeight);
    scroll.minimumZoomScale = 1.0f;
    scroll.zoomScale = minScale;
    scroll.maximumZoomScale = 4.0f;
}

#pragma mark- Guesture Methods
- (void)viewDidTap:(UITapGestureRecognizer*)sender
{
    [[self class] setActiveScrollView:self];
    
    NSNotification *n = [NSNotification notificationWithName:@"SelectScroll" object:self userInfo:nil];
    
    [[NSNotificationCenter defaultCenter]
     performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
}

-(void)handleTapGesture:(UITapGestureRecognizer*)sender
{
    NSNotification *n = [NSNotification notificationWithName:@"ChangeImage" object:self userInfo:nil];
    
    [[NSNotificationCenter defaultCenter]
     performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
}

@end

#pragma mark- _CLTextView

@implementation _CLTextViewEdit
{
    UILabel *_label;
    UIButton *_deleteButton;
    CLCircleView *_circleView;
    UIButton *_editButton;
    UIView *bgView;
    
    CGFloat _scale;
    CGFloat _arg;
    CGFloat _angle;
    
    CGPoint _initialPoint;
    CGFloat _initialArg;
    CGFloat _initialScale;
}

+ (void)setActiveTextView:(_CLTextViewEdit*)view
{
    static _CLTextViewEdit *activeView = nil;
    if(view != activeView){
        [activeView setAvtive:NO];
        activeView = view;
        [activeView setAvtive:YES];
        
        [activeView.superview bringSubviewToFront:activeView];
        
//        NSNotification *n = [NSNotification notificationWithName:@"CLTextViewActiveViewDidChangeNotificationStringEdit" object:view userInfo:nil];
//
//        [[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
    }
}

- (id)initWithTool:(CGRect)textFrame
{
    self = [super initWithFrame:textFrame];//2-100
    if(self){
        
        self.clipsToBounds = YES;
        
        bgView = [[UIView alloc] initWithFrame:_label.frame];
        bgView.layer.borderColor = [UIColor blackColor].CGColor;
        bgView.backgroundColor = [UIColor clearColor];
        bgView.layer.cornerRadius = 3;
        [self addSubview:bgView];
        
        _label = [[UILabel alloc] init];
        [_label setTextColor:[UIColor blackColor]];
        _label.numberOfLines = 0;
        _label.backgroundColor = [UIColor clearColor];
        //        _label.layer.borderColor = [[UIColor blackColor] CGColor];
        //        _label.layer.cornerRadius = 3;
//        _label.font = [UIFont fontWithName:@"Digitalt" size:([[NSUserDefaults standardUserDefaults] floatForKey:@"fontsize"])];
        //        _label.font = [UIFont systemFontOfSize:([[NSUserDefaults standardUserDefaults] floatForKey:@"fontsize"])];
        _label.shadowColor = [UIColor grayColor];
        _label.shadowOffset = CGSizeMake(0, 0);
        _label.minimumScaleFactor = 30/([[NSUserDefaults standardUserDefaults] floatForKey:@"fontsize"]);
        _label.adjustsFontSizeToFitWidth = YES;
        _label.textAlignment = NSTextAlignmentCenter;
        _label.clipsToBounds = YES;
        [self addSubview:_label];
        
        CGSize size = [_label sizeThatFits:CGSizeMake(FLT_MAX, FLT_MAX)];
        _label.frame = CGRectMake(16, 16, size.width, size.height);
        //        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width + 32, self.frame.size.height + 32);
        
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton setImage:[UIImage imageNamed:@"ic_delete"] forState:UIControlStateNormal];
        _deleteButton.frame = CGRectMake(0, 0, 30, 30);
        _deleteButton.layer.cornerRadius = 15.0;
        _deleteButton.center = _label.frame.origin;
        _deleteButton.backgroundColor = UIColor.clearColor;
        [_deleteButton addTarget:self action:@selector(pushedDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_deleteButton];
        
        _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editButton setImage:[UIImage imageNamed:@"ic_editText"] forState:UIControlStateNormal];
        _editButton.frame = CGRectMake(0, 0, 30, 30);
        _editButton.backgroundColor = [UIColor clearColor];
        _editButton.layer.cornerRadius = 15.0;
        _editButton.center = CGPointMake(_label.frame.size.width + _label.frame.origin.x, _label.frame.origin.y);
        //        _plusButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
        [_editButton addTarget:self action:@selector(pushedEditBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_editButton];
        [self bringSubviewToFront:_editButton];
        
        //                _circleView = [[CLCircleView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        //                _circleView.center = CGPointMake(_label.frame.size.width + _label.frame.origin.x, _label.frame.size.height + _label.frame.origin.y);
        //                _circleView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
        //                _circleView.layer.cornerRadius = 15.0;
        //                _circleView.backgroundColor = [UIColor whiteColor];
        //                _circleView.radius = 15;
        //                _circleView.color = [UIColor whiteColor];
        //                _circleView.borderColor = [UIColor blackColor];
        //                _circleView.borderWidth = 5;
        
        
        //        [self addSubview:_circleView];
        
        _arg = 0;
        _angle = 0;
        [self setScale:1];
        
        [self initGestures];
    }
    return self;
}

- (void)initGestures
{
    _label.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired = 2;
    [_label addGestureRecognizer:tapGesture];
    
    [_label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTap:)]];
    [_label addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidPan:)]];
    [_circleView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(circleViewDidPan:)]];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView* view= [super hitTest:point withEvent:event];
    if(view==self){
        return nil;
    }
    return view;
}

#pragma mark- Properties

- (void)setAvtive:(BOOL)active
{
    _deleteButton.hidden = !active;
    _circleView.hidden = !active;
    _editButton.hidden = !active;
    //    _label.layer.borderWidth = (active) ? 1/_scale : 0;
    bgView.layer.borderWidth = (active) ? 1/_scale : 0;
}

- (BOOL)active
{
    return !_deleteButton.hidden;
    return !_editButton.hidden;
}

- (void)sizeToFitWithMaxWidth:(CGFloat)width lineHeight:(CGFloat)lineHeight
{
    self.transform = CGAffineTransformIdentity;
    _label.transform = CGAffineTransformIdentity;
    bgView.transform = CGAffineTransformIdentity;
    
    CGSize size = [_label sizeThatFits:CGSizeMake(width / (([[NSUserDefaults standardUserDefaults] floatForKey:@"fontsize"])/([[NSUserDefaults standardUserDefaults] floatForKey:@"fontsize"])), FLT_MAX)];
    _label.frame = CGRectMake(16, 16, size.width, size.height);
    bgView.frame = CGRectMake(16, 16, size.width, size.height);
    
    CGFloat viewW = (_label.frame.size.width + 32);
    CGFloat viewH = _label.font.lineHeight;
    
    CGFloat ratio = MIN(width / viewW, lineHeight / viewH);
    [self setScale:ratio];
}

- (void)setScale:(CGFloat)scale
{
    _scale = scale;
    
    self.transform = CGAffineTransformIdentity;
    
    _label.transform = CGAffineTransformMakeScale(_scale, _scale);
    bgView.transform = CGAffineTransformMakeScale(_scale, _scale);
    
    CGRect rct = self.frame;
    rct.origin.x += (rct.size.width - (_label.frame.size.width + 32)) / 2;
    rct.origin.y += (rct.size.height - (_label.frame.size.height + 32)) / 2;
    rct.size.width  = _label.frame.size.width + 32;
    rct.size.height = _label.frame.size.height + 32;
    self.frame = rct;
    
    _label.center = CGPointMake(rct.size.width/2, rct.size.height/2);
    bgView.center = CGPointMake(rct.size.width/2, rct.size.height/2);
    
    _editButton.center = CGPointMake(_label.frame.size.width + _label.frame.origin.x, _label.frame.origin.y);
    
    self.transform = CGAffineTransformMakeRotation(_arg);
    
    //    _label.layer.borderWidth = 1/_scale;
    //    _label.layer.cornerRadius = 3/_scale;
    bgView.layer.borderWidth = 1/_scale;
    bgView.layer.cornerRadius = 3/_scale;
}

- (void)setFillColor:(UIColor *)fillColor
{
    _label.textColor = fillColor;
}

- (UIColor*)fillColor
{
    return _label.textColor;
}

- (void)setBorderColor:(UIColor *)borderColor
{
    //    _label.layer.borderColor = (__bridge CGColorRef _Nullable)(borderColor);
    bgView.layer.borderColor = (__bridge CGColorRef _Nullable)(borderColor);
}

- (UIColor*)borderColor
{
    //    return (CGColorRef)_label.layer.borderColor;
    return (CGColorRef)bgView.layer.borderColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
    //    _label.layer.borderWidth = borderWidth;
    bgView.layer.borderWidth = borderWidth;
}

- (CGFloat)borderWidth
{
    //    return _label.layer.borderWidth;
    return bgView.layer.borderWidth;
}

-(void)setFontColor:(UIColor *)color
{
    [_label setTextColor:color];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)setFontShadowColor:(UIColor *)colorShadow
{
    _label.layer.shadowColor = colorShadow.CGColor;
}


- (void)setFont:(UIFont *)font withBoundWidth:(CGFloat)boundWidth
{
    _label.font = font;
    
    NSString *text = _label.text;
    CGSize maximumSize = CGSizeMake(boundWidth, 9999);
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    CGSize textRect = [text boundingRectWithSize:maximumSize
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName: font,
                                                   NSParagraphStyleAttributeName:paragraphStyle}
                                         context:nil].size;
    CGFloat width = textRect.width;
    CGFloat height = textRect.height;
    
    [self sizeToFitWithMaxWidth:width lineHeight:height];
}

-(void)setAlpha:(CGFloat)alpha
{
    [_label setAlpha:alpha];
}

-(void)setAngle:(CGFloat)angle
{
    CGFloat radians = angle / 180.0 * M_PI_2;
    CGAffineTransform rotation = CGAffineTransformMakeRotation(radians);
    self.transform = rotation;
    _angle = angle;
    
}

-(CGFloat)angle
{
    return _angle;
}

-(NSString*)FontName
{
    return _label.font.fontName;
}

- (UIFont*)font
{
    return _label.font;
}

-(CGFloat)fontSize
{
    return _label.font.pointSize;
}

-(void)setShadowOffset:(CGSize)shadowOffset
{
    _label.shadowOffset = shadowOffset;
}

-(void)setShadowColor:(UIColor*)Color
{
    _label.shadowColor = Color; //default is (0.0, -3.0)
    //Default value is opaque black.
}

-(CGSize)shadowOffset
{
    return _label.shadowOffset;
}

-(UIColor*)shadowColor
{
    return _label.shadowColor;
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment
{
    _label.textAlignment = textAlignment;
}

- (NSTextAlignment)textAlignment
{
    return _label.textAlignment;
}

- (void)setText:(NSString *)text
{
    if(![text isEqualToString:_text]){
        _text = text;
        _label.lineBreakMode = NSLineBreakByWordWrapping;
        _label.text = (_text.length>0) ? _text : @"Begin writing your story here.";
    }
}

-(void)setLineSpace:(CGFloat)space wordSpace:(CGFloat)Wspace withMaxwidth:(CGFloat)maxWidth
{
    NSString *labelText = _label.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:_label.attributedText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:space];
    [paragraphStyle setAlignment:_label.textAlignment];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    [attributedString addAttribute:NSKernAttributeName
                             value:@(Wspace)
                             range:NSMakeRange(0, _label.text.length - 1)];
    _label.attributedText = attributedString ;
    [_label setAdjustsFontSizeToFitWidth:true];
    [_label sizeToFit];
    
    NSLog(@"_label Frame : %f",_label.frame.size.width);
    /*
     NSMutableAttributedString *attributedString1 = [[NSMutableAttributedString alloc] initWithString:_label.text];
     [attributedString1 addAttribute:NSKernAttributeName
     value:@(Wspace)
     range:NSMakeRange(0, _label.text.length)];
     
     _label.attributedText = attributedString;
     */
    
    CGSize maximumSize = CGSizeMake(maxWidth, 9999);
    
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle1.lineBreakMode = NSLineBreakByWordWrapping;
    _label.textAlignment = _label.textAlignment;
    
    CGSize textRect = [_label.text boundingRectWithSize:maximumSize
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{NSFontAttributeName: _label.font,
                                                          NSParagraphStyleAttributeName:paragraphStyle,
                                                          NSKernAttributeName:@(Wspace)
                                                          }
                                                context:nil].size;
    CGFloat width = textRect.width;
    CGFloat height = textRect.height;
    
    [self sizeToFitWithMaxWidth:width lineHeight:height];
}

-(void)setBlurShadow:(CGFloat)BlurRadius
{
    NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithAttributedString:_label.attributedText];
    NSShadow * shadow = [[NSShadow alloc] init];
    shadow.shadowColor = _label.shadowColor;
    shadow.shadowBlurRadius = BlurRadius;
    shadow.shadowOffset = _label.shadowOffset;
    NSDictionary * attris = @{NSShadowAttributeName:shadow};
    //    [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
    [mutableAttriStr addAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
    _label.attributedText = mutableAttriStr;
}

-(void)setTextStrock:(CGFloat)strock StrockColor:(UIColor *)color
{
    
    NSString *labelText = _label.text;
    
    NSMutableAttributedString *temp = [[NSMutableAttributedString alloc] initWithAttributedString:_label.attributedText];
    [temp addAttribute:NSStrokeWidthAttributeName value:@(-strock) range:NSMakeRange(0, [labelText length])];
    [temp addAttribute:NSStrokeColorAttributeName value:color range:NSMakeRange(0, [labelText length])];
    [temp addAttribute:NSForegroundColorAttributeName value:_label.textColor range:NSMakeRange(0, [labelText length])];
    
    _label.attributedText = temp;
}

- (void)pushedEditBtn:(id)sender
{
    NSNotification *n = [NSNotification notificationWithName:@"CLTextViewActiveViewDidTapNotificationString" object:self userInfo:nil];
    
    [[NSNotificationCenter defaultCenter]
     performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
}

- (void)pushedDeleteBtn:(id)sender
{
    _CLTextViewEdit *nextTarget = nil;
    
    const NSInteger index = [self.superview.subviews indexOfObject:self];
    
    for(NSInteger i=index+1; i<self.superview.subviews.count; ++i){
        UIView *view = [self.superview.subviews objectAtIndex:i];
        if([view isKindOfClass:[_CLTextViewEdit class]]){
            nextTarget = (_CLTextViewEdit*)view;
            break;
        }
    }
    
    if(nextTarget==nil){
        for(NSInteger i=index-1; i>=0; --i){
            UIView *view = [self.superview.subviews objectAtIndex:i];
            if([view isKindOfClass:[_CLTextViewEdit class]]){
                nextTarget = (_CLTextViewEdit*)view;
                break;
            }
        }
    }
    
//    [[self class] setActiveTextView:nextTarget];
    
//    if(nextTarget==nil){
        NSNotification *n = [NSNotification notificationWithName:@"deleteText" object:self userInfo:nil];
        
        [[NSNotificationCenter defaultCenter]
         performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
//    }
    [self removeFromSuperview];
}

#pragma mark- gesture events

- (void)viewDidTap:(UITapGestureRecognizer*)sender
{
    [[self class] setActiveTextView:self];
    if(self.active){
        NSNotification *n = [NSNotification notificationWithName:@"CLTextViewActiveViewDidChangeNotificationStringEdit" object:self userInfo:nil];
        
        [[NSNotificationCenter defaultCenter]
         performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
    }
    
    
    BOOL temp = [[NSUserDefaults standardUserDefaults] boolForKey:@"textView"];
    
    if (temp)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"textView"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)handleTapGesture:(UITapGestureRecognizer*)sender
{
    NSNotification *n = [NSNotification notificationWithName:@"CLTextViewActiveViewDidTapNotificationString" object:self userInfo:nil];
    
    [[NSNotificationCenter defaultCenter]
     performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
}

- (void)viewDidPan:(UIPanGestureRecognizer*)sender
{
    //    [[self class] setActiveTextView:self];
    
    CGPoint p = [sender translationInView:self.superview];
    
    if(sender.state == UIGestureRecognizerStateBegan){
        _initialPoint = self.center;
    }
    self.center = CGPointMake(_initialPoint.x + p.x, _initialPoint.y + p.y);
    
    
    //    CGRect boundsRect = CGRectMake(0, 0, self.superview.frame.size.width, self.superview.frame.size.height-50);
    //
    //    if (self.frame.origin.x > 0 && self.frame.origin.y > 0 && CGRectContainsPoint(boundsRect, self.center))
    //    {
    //        self.center = CGPointMake(_initialPoint.x + p.x, _initialPoint.y + p.y);
    //    }
    //
    //    if (CGRectContainsPoint(boundsRect, self.center)) {
    //        self.center = CGPointMake(_initialPoint.x + p.x, _initialPoint.y + p.y);
    //
    //    }
    
}

- (void)circleViewDidPan:(UIPanGestureRecognizer*)sender
{
    CGPoint p = [sender translationInView:self.superview];
    
    static CGFloat tmpR = 1;
    static CGFloat tmpA = 0;
    if(sender.state == UIGestureRecognizerStateBegan){
        _initialPoint = [self.superview convertPoint:_circleView.center fromView:_circleView.superview];
        
        CGPoint p = CGPointMake(_initialPoint.x - self.center.x, _initialPoint.y - self.center.y);
        tmpR = sqrt(p.x*p.x + p.y*p.y);
        tmpA = atan2(p.y, p.x);
        
        _initialArg = _arg;
        _initialScale = _scale;
    }
    
    p = CGPointMake(_initialPoint.x + p.x - self.center.x, _initialPoint.y + p.y - self.center.y);
    CGFloat R = sqrt(p.x*p.x + p.y*p.y);
    CGFloat arg = atan2(p.y, p.x);
    
    _arg   = _initialArg + arg - tmpA;
    [self setScale:MAX(_initialScale * R / tmpR, 30/([[NSUserDefaults standardUserDefaults] floatForKey:@"fontsize"]))];
}

@end
