//
//  StoryTellerVCViewController.h
//  StoryTeller
//
//  Created by harshesh on 27/04/19.
//  Copyright © 2019 nectarbits. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StoryTellerVC : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIImagePickerController *ipc;
    IBOutlet UILabel *lblCounter;

}
@property (strong, nonatomic) IBOutlet UICollectionView *clevStoryTeller;
@property (strong, nonatomic) IBOutlet UIButton *btnAddFrame;
@property (strong, nonatomic) IBOutlet UIButton *btnMenu;
@property (strong, nonatomic) IBOutlet UIButton *btnShow;
@property (strong, nonatomic) IBOutlet UIButton *btnDeleteFrame;
@property (strong, nonatomic) IBOutlet UIButton *btnDownload;

@property (strong, nonatomic) IBOutlet UIView *viewEdit;
@property (strong, nonatomic) IBOutlet UITextView *txtEditView;
@property (strong, nonatomic) IBOutlet UIButton *btnTextEditCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnTextEditDone;
@property (strong, nonatomic) IBOutlet UILabel *lblNoStory;

@property (strong, nonatomic) IBOutlet UIView *viewShare;
@property (strong, nonatomic) IBOutlet UIView *viewShareCard;
@property (strong, nonatomic) IBOutlet UIButton *btnPage;
@property (strong, nonatomic) IBOutlet UIButton *btnStory;
@property (strong, nonatomic) IBOutlet UIButton *btnShareToInstagram;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveCancel;

@property (strong, nonatomic) IBOutlet UIView *viewDelete;
@property (strong, nonatomic) IBOutlet UIView *viewDeleteCard;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UIButton *btnDeleteCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnDeleteClose;


@property (nonatomic, retain) UIDocumentInteractionController *documentController;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *TopHeightConstraint;

@end

NS_ASSUME_NONNULL_END
