//
//  StickerCell.h
//  Typogrphy
//
//  Created by NectarBits on 03/01/18.
//  Copyright © 2018 NectarBits. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StickerCell : UICollectionViewCell

@property(nonatomic, strong)UIImageView *imgSticker;
@property(nonatomic, strong)UIImageView *imgSelection;
@end
