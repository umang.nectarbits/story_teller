//
//  SelectFrameViewTool.h
//  StoryTeller
//
//  Created by nectarbits on 5/1/19.
//  Copyright © 2019 nectarbits. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SelectFrameViewToolDelegate;

@interface SelectFrameViewTool : UIView <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong)UICollectionView *collColorView;
@property (nonatomic, strong)NSMutableArray *FrameArray;
@property (nonatomic, strong)NSMutableArray *TextArray;
@property (nonatomic, strong)NSMutableArray *frameImageArr;
@property (nonatomic, strong)NSString *strSelectedCategory;
@property (nonatomic, weak) id<SelectFrameViewToolDelegate> delegate;
@property (nonatomic, strong) UIButton *btnDownload;

-(void)setBackgroundCategory;
@end

@protocol SelectFrameViewToolDelegate <NSObject>

-(void)selectedFrame:(NSMutableArray*)frameArr text:(NSMutableArray*)textArray;
-(void)hideSelectedFrameView;
@end


NS_ASSUME_NONNULL_END
