//
//  StickerCell.m
//  Typogrphy
//
//  Created by NectarBits on 03/01/18.
//  Copyright © 2018 NectarBits. All rights reserved.
//

#import "StickerCell.h"


@implementation StickerCell
@synthesize imgSticker;




- (instancetype)initWithFrame:(CGRect)rect {
    
    self = [super initWithFrame:rect];
    
    if (self) {
        imgSticker = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
        imgSticker.contentMode = UIViewContentModeScaleAspectFit;
//        imgSticker.clipsToBounds = YES;
//        [imgSticker setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:imgSticker];
        
        _imgSelection = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
        _imgSelection.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_imgSelection];
    }
    
    
    return self;
}

//- (id)init
//{
//    self = [super init];
//    if (self) {
//        
//        imgSticker = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
//        imgSticker.contentMode = UIViewContentModeScaleAspectFill;
//        imgSticker.clipsToBounds = YES;
//        [imgSticker setTranslatesAutoresizingMaskIntoConstraints:NO];
//        [self.contentView addSubview:imgSticker];
//    }
//    
//    return self;
//}


@end
