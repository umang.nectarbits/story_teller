//
//  SelectFrameViewTool.m
//  StoryTeller
//
//  Created by nectarbits on 5/1/19.
//  Copyright © 2019 nectarbits. All rights reserved.
//

#import "SelectFrameViewTool.h"
#import "StickerCell.h"

@interface SelectFrameViewTool()
{
    NSString *selectedBG;
}
@end

@implementation SelectFrameViewTool

@synthesize collColorView,strSelectedCategory,btnDownload,delegate,FrameArray;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.clipsToBounds = YES;
        
        self.frame = frame;
        
        UICollectionViewFlowLayout *layout1 = [[UICollectionViewFlowLayout alloc] init];
        [layout1 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        //225
        collColorView =[[UICollectionView alloc] initWithFrame:CGRectMake(0, 40, self.frame.size.width,  self.frame.size.height - 65) collectionViewLayout:layout1];
        [collColorView setDataSource:self];
        [collColorView setDelegate:self];
        
        layout1.minimumLineSpacing = 10.0f;
        layout1.minimumInteritemSpacing = 5.0f;
        [collColorView registerClass:[StickerCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collColorView setBackgroundColor:[UIColor blackColor]];
        [collColorView setShowsVerticalScrollIndicator:NO];
        [self addSubview:collColorView];
        
        UIButton *btnDown = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDown addTarget:self
                    action:@selector(BtnDownPressed:)
          forControlEvents:UIControlEventTouchUpInside];
        btnDown.frame = CGRectMake(0.0, collColorView.frame.origin.y + collColorView.frame.size.height, self.frame.size.width, 25);
        btnDown.backgroundColor = [UIColor blackColor];
        [btnDown setImage:[UIImage imageNamed:@"ic_downArrow"] forState:UIControlStateNormal];
        [self addSubview:btnDown];
        
        strSelectedCategory = @"Color";
        
        _frameImageArr = [[NSMutableArray alloc] initWithObjects:@"ic_F1",@"ic_F2",@"ic_F4",@"ic_F3",@"ic_F5",@"ic_F6",@"ic_F7",@"ic_F8",@"ic_F9",@"ic_F10",@"ic_F11",@"ic_F12",@"ic_F13",@"ic_F14",nil];
        _TextArray = [[NSMutableArray alloc] init];
        
        [self loadStoryFrame];
        
        [collColorView reloadData];
        
    }
    
    return self;
}


-(void)loadStoryFrame{
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"FrameJSON" ofType:@"txt"];
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    NSAssert(myJSON, @"File FrameJSON.txt couldn't be read!");
    
    NSData *data = [myJSON dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *dict =[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    FrameArray =[[NSMutableArray alloc] initWithCapacity:4];
    for (NSDictionary *i in [[dict objectForKey:@"StoryTeller_Frame"] objectForKey:@"Frame"]) {
        [FrameArray addObject:i];
    }
//    [self addScrollsWithIndex:frameIndex];
}


//MARK:- CollectionView Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _frameImageArr.count;
  
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StickerCell *cell = [collColorView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.backgroundColor=[UIColor clearColor];
    
    [[[cell.imgSticker.layer sublayers] objectAtIndex:0] removeFromSuperlayer];
    cell.imgSticker.image = [UIImage imageNamed:_frameImageArr[indexPath.row]];
//    cell.imgSticker.backgroundColor = [self colorWithHexString:[colorArr objectAtIndex:indexPath.row]];
    
//    cell.layer.cornerRadius = 4.0f;
    cell.imgSelection.layer.cornerRadius = 4.0f;
    cell.imgSelection.layer.masksToBounds = YES;
    cell.imgSticker.layer.cornerRadius = 4.0f;
    cell.imgSticker.layer.masksToBounds = YES;
//    cell.layer.shadowRadius  = 4.0f;
//    cell.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
//    cell.layer.shadowOffset  = CGSizeZero;
//    cell.layer.shadowOpacity = 1.2f;
    cell.layer.masksToBounds = NO;

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(50, 100);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(10, 5, 10, 5);// top, left, bottom, right
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [delegate selectedFrame:FrameArray[indexPath.row][@"scrolls"] text:FrameArray[indexPath.row][@"isText"]];
    [collColorView reloadData];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)setBackgroundCategory
{
    [collColorView setHidden:NO];
    collColorView.frame = CGRectMake(0, 40, self.frame.size.width, self.frame.size.height - 65);
    [collColorView reloadData];
}

//MARK:- Action Methods
-(void)BtnDownPressed:(UIButton*)sender
{
    NSLog(@"you clicked on button %@", sender.tag);
    [delegate hideSelectedFrameView];
}

@end
