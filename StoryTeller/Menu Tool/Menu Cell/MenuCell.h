//
//  MenuCell.h
//  GPUImage_Objective_C
//
//  Created by Cools on 1/4/18.
//  Copyright © 2018 Cools. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UICollectionViewCell
@property(nonatomic, strong)UIImageView *imgMenu;
@property(nonatomic, strong)UILabel *lblTitle;
@end
