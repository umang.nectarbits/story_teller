//
//  MenuCell.m
//  GPUImage_Objective_C
//
//  Created by Cools on 1/4/18.
//  Copyright © 2018 Cools. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell
@synthesize imgMenu,lblTitle;

- (instancetype)initWithFrame:(CGRect)rect {
    
    self = [super initWithFrame:rect];
    
    if (self) {
       /* imgMenu = [[UIImageView alloc] initWithFrame:CGRectMake((rect.size.width/2)-15, 10, 30, 50)];
        imgMenu.contentMode = UIViewContentModeScaleAspectFit;
        imgMenu.clipsToBounds = YES;
        [imgMenu setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:imgMenu]; */
        
        imgMenu = [[UIImageView alloc] initWithFrame:CGRectMake((rect.size.width/2)-22.5, 5, 45, 45)];
        imgMenu.contentMode = UIViewContentModeScaleAspectFit;
        imgMenu.clipsToBounds = YES;
        [imgMenu setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:imgMenu];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake((rect.size.width/2)-25, 55, 50, 15)];
//        lblTitle.font = [UIFont fontWithName:FONTROBOTOLIGHT size:5.0];
        [lblTitle setFont:[UIFont systemFontOfSize:12]];
        lblTitle.textColor = UIColor.whiteColor;
        lblTitle.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:lblTitle];
        
    }
    
    return self;
}

@end
