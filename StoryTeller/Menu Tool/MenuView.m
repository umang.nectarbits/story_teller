//
//  MenuView.m
//  StoryTeller
//
//  Created by nectarbits on 5/2/19.
//  Copyright © 2019 nectarbits. All rights reserved.
//

#import "MenuView.h"
#import "MenuCell.h"

@implementation MenuView

@synthesize collMenuView,menuArray,delegate,menuTitleArray,btnMenuClose;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        
        self.frame = frame;
        
        self.backgroundColor = UIColor.blackColor;
        
        self.layer.shadowRadius  = 4.0f;
        self.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
        self.layer.shadowOffset  = CGSizeZero;
        self.layer.shadowOpacity = 1.2f;
        self.layer.masksToBounds = NO;
        
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        
        collMenuView =[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 80) collectionViewLayout:layout];
        
        [collMenuView setDataSource:self];
        [collMenuView setDelegate:self];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 0.0f;
        
        [collMenuView setShowsHorizontalScrollIndicator:NO];
        /* [collStickerView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];*/
        [collMenuView registerClass:[MenuCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collMenuView setClipsToBounds:YES];
        collMenuView.backgroundColor = [UIColor blackColor];
        [self addSubview:collMenuView];
        
        btnMenuClose = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnMenuClose addTarget:self
                    action:@selector(aBtnDownPressed:)
          forControlEvents:UIControlEventTouchUpInside];
        btnMenuClose.frame = CGRectMake(0.0, 55, self.frame.size.width, 25);
        btnMenuClose.backgroundColor = [UIColor clearColor];
        [btnMenuClose setImage:[UIImage imageNamed:@"ic_downArrow"] forState:UIControlStateNormal];
        [self addSubview:btnMenuClose];
        [self bringSubviewToFront:btnMenuClose];
        
        self.menuArray = [self.class allEmojiList];
     
        self.menuTitleArray = [NSArray arrayWithObjects:@"Color",@"Text", nil];
        selectedIndex = 0;
        [collMenuView reloadData];
    }
    return self;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return menuArray.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /*  UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];*/
    
    MenuCell *cell = [collMenuView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.imgMenu.image = [UIImage imageNamed:menuArray[indexPath.row]];
    cell.imgMenu.contentMode = UIViewContentModeScaleAspectFill;
    cell.lblTitle.text = menuTitleArray[indexPath.row];
    // cell.lblTitle.text = menuTitleArray[indexPath.row];
    cell.highlighted = YES;
    cell.backgroundColor = UIColor.blackColor;
    //    if (selectedIndex == indexPath.row)
    //    {
    //        cell.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:244.0/255.0 blue:248.0/255.0 alpha:1];
    //    }
    //    else
    //    {
    //        cell.backgroundColor = [UIColor whiteColor];
    //    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // return CGSizeMake([[UIScreen mainScreen] bounds].size.width/5, 80);
    if (menuArray.count >= 5)
    {
        return CGSizeMake([[UIScreen mainScreen] bounds].size.width/5, 80);
    }
    else
    {
        return CGSizeMake([[UIScreen mainScreen] bounds].size.width/menuArray.count, 80);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //    selectedIndex = indexPath.row;
    //    [collectionView reloadData];
    [self getSelectedIndexMenu:indexPath.row];
}

//-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
//    MenuCell *cell = (MenuCell*)[collMenuView cellForItemAtIndexPath:indexPath];
//    [UIView animateWithDuration:2.0 animations:^{
//        cell.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1];
//    } completion:NULL];
//}
//
//-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath{
//    MenuCell *cell = (MenuCell*)[collMenuView cellForItemAtIndexPath:indexPath];
//    [UIView animateWithDuration:2.0 animations:^{
//        cell.backgroundColor = [UIColor whiteColor];
//    } completion:NULL];
//
//}


-(void)aBtnDownPressed:(UIButton*)sender
{
    //NSLog(@"you clicked on button %@", sender.tag);
    [delegate hideMenuTool];
}

- (void)getSelectedIndexMenu:(NSInteger)index
{
    NSLog(@"%ld",(long)index);
    [delegate SelectedIndexMenu:index];
}


+ (NSArray*)allEmojiList
{
    //    NSArray  *tempEmojiArray = [NSArray arrayWithObjects:@"ic_Poster",@"ic_filter-1",@"ic_BG",@"ic_blurr",@"ic_art",@"ic_text-1", nil];
    NSArray  *tempEmojiArray = [NSArray arrayWithObjects:@"ic_color",@"ic_text", nil];
    
    return tempEmojiArray;
}


@end
