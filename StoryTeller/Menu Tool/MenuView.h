//
//  MenuView.h
//  StoryTeller
//
//  Created by nectarbits on 5/2/19.
//  Copyright © 2019 nectarbits. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuClassDelegate;


@interface MenuView : UIView <UICollectionViewDelegate, UICollectionViewDataSource>
{
    NSInteger selectedIndex;
}

@property (nonatomic, weak) id<MenuClassDelegate> delegate;
@property (nonatomic, strong)UICollectionView *collMenuView;
@property (nonatomic, strong)UIButton *btnMenuClose;
@property (nonatomic, strong)NSArray *menuArray;
@property (nonatomic, strong)NSArray *menuTitleArray;
@end

@protocol MenuClassDelegate <NSObject>
- (void)SelectedIndexMenu:(NSInteger)index;
-(void)hideMenuTool;
@end
