//
//  AppUtility.m
//  Typogrphy
//
//  Created by NectarBits on 09/01/18.
//  Copyright © 2018 NectarBits. All rights reserved.
//

#import "AppUtility.h"
#import "MBProgressHUD.h"

@implementation AppUtility

+ (AppUtility *)sharedInstance {
    static AppUtility *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[AppUtility alloc] init];
    });
    
    return _sharedInstance;
}

#pragma mark:- WS

+(void)show:(UIView *)view {
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:view animated:YES];
    });
}

+(void)hide:(UIView *)view {
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:view animated:YES];
    });
}

@end
