//
//  Constant.h
//  Typogrphy
//
//  Created by NectarBits on 09/01/18.
//  Copyright © 2018 NectarBits. All rights reserved.
//
#define APPNAME                   @"Post.Art"
#define InternetConnection  @"Please check your internet connection."
#define SomethingWrong  @"Something went wrong. Please try again later."
#define CATEGORY_DOWNLOAD_NAME    @"Download"
#define APP_STORE_ID              1440118452

//for Display in Appstore
//https://itunes.apple.com/us/app/post-art-design-poster-maker/id1440118452?ls=1&mt=8

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define BANNER_AD_ID        @"ca-app-pub-3258200689610307/6803900782"
#define INTERSTITIAL_AD_ID  @"ca-app-pub-3258200689610307/8555966152"

#define GETPOSTER                 @"http://iappbits.in/photoapp/poster/getPoster/"
//#define GETPOSTER                 @"http://192.168.1.60/photoapp_new/poster/getPostercat"

#define GETFONT                   @"http://iappbits.in/photoapp/font/getFontdata/"
#define GETFILTERDATA             @"http://iappbits.in/photoapp/Filter/getFilterData_v1"
#define GETBACKGROUND             @"http://iappbits.in/photoapp/background/getBackgrounddata/"
#define GETFRAME                  @"http://iappbits.in/photoapp/frame/getFramedata/"
#define GETARTS                   @"http://iappbits.in/photoapp/sticker/getSticker"
#define IMGURL                    @"http://iappbits.in/photoapp/uploads/poster/poster/"

#define SCREEN_HEIGHT             UIScreen.mainScreen.nativeBounds.size.height
#define SCREEN_WIDTH              UIScreen.mainScreen.nativeBounds.size.width

#define USERDEFAULTFRAME          @"FrameImages"
#define USERDEFAULTBACKGROUND     @"BackgroundImages"
#define USERDEFAULTFILTER         @"FilterImages"
#define USERDEFAULTARTS           @"ArtsImages"
#define USERDEFAULTFONTS          @"Fonts"
#define USERDEFAULTAllDOWNLOAD    @"AllCategoryDownload"
#define USERDEFAULTDOWNLOAD       @"Download"
#define USERDEFAULTREMOVEAD       @"RemoveAd"
#define USERDEFAULTRESTORED       @"Restored"
#define USERDEFAULTPROSUBSCRIPTION  @"PROSubscription"
#define PURCHASERECIEPT           @"PurchaseReciept"
#define SHAREDSECRETKEY           @"18264cffe85641fbb2c88fcddca8354f"

//#define FONTSANFRANCISCOBOLD      @".SFUIDisplay-Bold"
//#define FONTSANFRANCISCOMEDIUM    @".SFUIDisplay-Medium"

#define FONTSANFRANCISCOREGULAR   @"SanFranciscoDisplay-Regular"
#define FONTSANFRANCISCOMEDIUM    @"SanFranciscoDisplay-Medium"
#define FONTSANFRANCISCOLIGHT     @"SanFranciscoDisplay-Light"

#define FONTROBOTOLIGHT           @"Roboto-Light"
#define FONTROBOTOREGULAR         @"Roboto-Regular"
#define FONTROBOTOTHIN            @"Roboto-Thin"
#define FONTROBOTOBOLD            @"Roboto-Bold"

#define NOTIFICATION_KEY          @"NOtificationKey"
#define RESOLUTION_KEY            @"ResolutionKey"
#define AUTO_SAVE_KEY             @"AutoSaveKey"

#define HEADERCOLOR               @"2A2D31"

//Declare Filter Sample Image
#define FILTERIMAGE                @"sampleImg.png"
#define MENUTITLEBLACKCOLOR       [UIColor blackColor]

#define DOWNLOADBUTTONCOLOR       [UIColor colorWithRed:64.0/255.0 green:224.0/255.0 blue:208.0/255.0 alpha:1]
