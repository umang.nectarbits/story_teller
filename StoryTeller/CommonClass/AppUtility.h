//
//  AppUtility.h
//  Typogrphy
//
//  Created by NectarBits on 09/01/18.
//  Copyright © 2018 NectarBits. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AppUtility : NSObject

+ (AppUtility *)sharedInstance;
+(void)show:(UIView *)view;
+(void)hide:(UIView *)view;

@end
