//
//  FontStyleClass.m
//  Typogrphy
//
//  Created by NectarBits on 05/01/18.
//  Copyright © 2018 NectarBits. All rights reserved.
//

#import "FontStyleClass.h"
#import "CategoryCell.h"
#import "FontCell.h"
#import "StickerCell.h"
#import <CoreText/CoreText.h>
#import "AFNetworking.h"
#import "Constant.h"

@implementation FontStyleClass
@synthesize viewBtn,collCategoryView,collColorView,tblFonts,categoryArr,ColorArray,arrFontStyle,sliderFontSize,sliderFontShadow,viewSlider,delegate,strColorType,scrollView,viewFontEdit,viewFontColor,viewFontStyle,collShadowColor,collFontCategoryView,fontCategoryArr,selectedFontCategoryArr,strFontType,allFontStyleArr,SliderRotationX,SliderRotationY,view3D,scrollFontStyle, lblLineSpacing, lblWordsSpacing, SliderLineSpacing, SliderWordSpacing,lblBlurShadow,SliderBlurShadow,btnShadowAngleRight,btnShadowAngleLeft,btnShadowAngleTop,btnShadowAngleBotton,btnAlignLeft,btnAlignRight,btnAlignCenter,viewTextOutline,SliderStrockSize,lblTextStrock,collStrockColor,lblTextAlpha,SliderTextAlpha,viewTextAngle,lblTextAngle,SliderTextAngle;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        
        self.frame = frame;
        self.backgroundColor = [UIColor blackColor];
        
        /*
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        
        collCategoryView =[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40) collectionViewLayout:layout];
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [collCategoryView setDataSource:self];
        layout.minimumLineSpacing = 0.0f;
        
        [collCategoryView setDelegate:self];
        [collCategoryView registerClass:[CategoryCell class] forCellWithReuseIdentifier:@"cellCategory"];
        [collCategoryView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:collCategoryView];
        */
        
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 40, self.frame.size.width, 160)];
        scrollView.delegate = self;
        [scrollView setPagingEnabled:YES];
        [scrollView setBounces:NO];
        scrollView.scrollEnabled = NO;
        scrollView.backgroundColor = [UIColor clearColor];
        [self addSubview:scrollView];
        
        //View For Font Style
        viewFontStyle = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, scrollView.frame.size.height)];
        viewFontStyle.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:viewFontStyle];
        
        UICollectionViewFlowLayout *layout3=[[UICollectionViewFlowLayout alloc] init];
        collFontCategoryView =[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0) collectionViewLayout:layout3];
        [layout3 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [collFontCategoryView setDataSource:self];
        layout3.minimumLineSpacing = 0.0f;
        [collFontCategoryView setDelegate:self];
        [collFontCategoryView registerClass:[CategoryCell class] forCellWithReuseIdentifier:@"cellFontCategory"];
        [collFontCategoryView setBackgroundColor:[UIColor clearColor]];
        [viewFontStyle addSubview:collFontCategoryView];
        
        _scrollMenu = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
        _scrollMenu.backgroundColor = [UIColor clearColor];
        _scrollMenu.indicatorTintColor = [UIColor whiteColor];
        _scrollMenu.hasShadowForBoth = NO;
        _scrollMenu.shouldUniformizeMenus = YES;
        _scrollMenu.delegate = self;
        [self addSubview:self.scrollMenu];
        
        //Table For Font Style
        tblFonts = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, viewFontStyle.frame.size.height - 25)];
        [tblFonts setDelegate:self];
        [tblFonts setDataSource:self];
        [tblFonts registerClass:[FontCell class] forCellReuseIdentifier:@"FontCell"];
        tblFonts.rowHeight = 45;
        tblFonts.sectionFooterHeight = 22;
        tblFonts.sectionHeaderHeight = 22;
        tblFonts.scrollEnabled = YES;
        tblFonts.separatorStyle = UITableViewCellSeparatorStyleNone;
        tblFonts.userInteractionEnabled = YES;
        tblFonts.bounces = YES;
        tblFonts.backgroundColor = [UIColor clearColor];
        strFontType = @"Font";
        [viewFontStyle addSubview:tblFonts];
        
        //View For Font Color
        viewFontColor = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width, 0, self.frame.size.width, scrollView.frame.size.height)];
        viewFontColor.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:viewFontColor];
        
        //Color Collection View
        
        btnAlignLeft = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnAlignLeft addTarget:self
                                action:@selector(btnAlignLeftPressed:)
                      forControlEvents:UIControlEventTouchUpInside];
        btnAlignLeft.frame = CGRectMake((viewFontColor.frame.size.width / 2) - 110, 40, 30, 30);
        [btnAlignLeft setImage:[UIImage imageNamed:@"ic_AlignLeft"] forState:UIControlStateNormal];
        [btnAlignLeft setImage:[UIImage imageNamed:@"ic_AlignLeftSelect"] forState:UIControlStateSelected];
        [viewFontColor addSubview:btnAlignLeft];
        
        btnAlignCenter = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnAlignCenter addTarget:self
                                action:@selector(btnAlignCenterPressed:)
                      forControlEvents:UIControlEventTouchUpInside];
        btnAlignCenter.frame = CGRectMake((viewFontColor.frame.size.width / 2) - 15, 40, 30, 30);
        [btnAlignCenter setImage:[UIImage imageNamed:@"ic_AlignCenter"] forState:UIControlStateNormal];
        [btnAlignCenter setImage:[UIImage imageNamed:@"ic_AlignCenterSelect"] forState:UIControlStateSelected];
        [btnAlignCenter setSelected:YES];
        [viewFontColor addSubview:btnAlignCenter];
        
        btnAlignRight = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnAlignRight addTarget:self
                                action:@selector(btnAlignRightPressed:)
                      forControlEvents:UIControlEventTouchUpInside];
        btnAlignRight.frame = CGRectMake((viewFontColor.frame.size.width / 2) + 80, 40, 30, 30);
        [btnAlignRight setImage:[UIImage imageNamed:@"ic_AlignRight"] forState:UIControlStateNormal];
        [btnAlignRight setImage:[UIImage imageNamed:@"ic_AlignRightSelect"] forState:UIControlStateSelected];
        [viewFontColor addSubview:btnAlignRight];
        
        
        UICollectionViewFlowLayout *layout1=[[UICollectionViewFlowLayout alloc] init];
        [layout1 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        collColorView =[[UICollectionView alloc] initWithFrame:CGRectMake(0, 70, self.frame.size.width, 80) collectionViewLayout:layout1];
        layout1.minimumLineSpacing = 10.0f;
        layout1.minimumInteritemSpacing = 5.0f;
        [collColorView setDataSource:self];
        [collColorView setDelegate:self];
        [collColorView registerClass:[StickerCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collColorView setBackgroundColor:[UIColor clearColor]];
        [viewFontColor addSubview:collColorView];
        
        //View For Font Color
        viewFontEdit = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width*2, 0, self.frame.size.width, scrollView.frame.size.height)];
        viewFontEdit.backgroundColor = [UIColor clearColor];
        [scrollView addSubview:viewFontEdit];
        
        viewSlider = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.frame.size.width, 160)];
        viewSlider.backgroundColor = [UIColor clearColor];
        [viewFontEdit addSubview:viewSlider];
        
        scrollFontStyle = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.viewSlider.frame.size.width, self.viewSlider.frame.size.height)];
        scrollFontStyle.delegate = self;
//        [scrollFontStyle setPagingEnabled:YES];
//        [scrollFontStyle setBounces:NO];
        scrollFontStyle.userInteractionEnabled= YES;
        scrollFontStyle.scrollEnabled = YES;
        scrollFontStyle.backgroundColor = [UIColor clearColor];
        [viewSlider addSubview:scrollFontStyle];
        scrollFontStyle.contentSize= CGSizeMake(self.viewSlider.frame.size.width ,150);
        
        
        UILabel *lblFontSize = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 130, 30)];
        lblFontSize.text = @"Font Size :";
        [lblFontSize setFont:[UIFont fontWithName:FONTROBOTOREGULAR size:15.0]];
        [lblFontSize setTextColor:[UIColor whiteColor]];
        [viewFontColor addSubview:lblFontSize];
        selectedIndex = 0;
        selectedFontIndex = 0;
        
        CGRect frame = CGRectMake(140, 5, scrollFontStyle.frame.size.width/2 - 30, 30.0);
        sliderFontSize = [[UISlider alloc] initWithFrame:frame];
        sliderFontSize.minimumTrackTintColor = [UIColor whiteColor];
        sliderFontSize.maximumTrackTintColor = [UIColor grayColor];
        [sliderFontSize setThumbImage:[UIImage imageNamed:@"sliderImg"] forState:UIControlStateNormal];
        [sliderFontSize addTarget:self action:@selector(onSliderValChanged:forEvent:) forControlEvents:UIControlEventValueChanged];
        [sliderFontSize setBackgroundColor:[UIColor clearColor]];
        sliderFontSize.minimumValue = 15.0;
        sliderFontSize.maximumValue = 50.0;
        sliderFontSize.continuous = YES;
        sliderFontSize.value = 30.0;
        [viewFontColor addSubview:sliderFontSize];
        
        lblBlurShadow = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 130, 30)];
        lblBlurShadow.text = @"Blur :";
        lblBlurShadow.textAlignment = NSTextAlignmentCenter;
        [lblBlurShadow setTextColor:[UIColor whiteColor]];
        [lblBlurShadow setFont:[UIFont fontWithName:FONTROBOTOREGULAR size:15.0]];
        [scrollFontStyle addSubview:lblBlurShadow];
       
        
        CGRect frame3 = CGRectMake(10, 30, scrollFontStyle.frame.size.width - 200, 20);
        SliderBlurShadow = [[UISlider alloc] initWithFrame:frame3];
        [SliderBlurShadow addTarget:self action:@selector(setSliderBlurShadow:) forControlEvents:UIControlEventValueChanged];
        SliderBlurShadow.minimumTrackTintColor = [UIColor whiteColor];
        SliderBlurShadow.maximumTrackTintColor = [UIColor grayColor];
        [SliderBlurShadow setThumbImage:[UIImage imageNamed:@"sliderImg"] forState:UIControlStateNormal];
        [SliderBlurShadow setBackgroundColor:[UIColor clearColor]];
        SliderBlurShadow.minimumValue = 0;
        SliderBlurShadow.maximumValue = 7;
        SliderBlurShadow.continuous = YES;
        SliderBlurShadow.value = 0;
        [scrollFontStyle addSubview:SliderBlurShadow];
        
        
        UILabel *lblFontShadow = [[UILabel alloc] initWithFrame:CGRectMake((scrollFontStyle.frame.size.width /2) + 20, 0, 130, 30)];
        lblFontShadow.text = @"Font Shadow :";
        lblFontShadow.textAlignment = NSTextAlignmentCenter;
        [lblFontShadow setTextColor:[UIColor whiteColor]];
        [lblFontShadow setFont:[UIFont fontWithName:FONTROBOTOREGULAR size:15.0]];
        [scrollFontStyle addSubview:lblFontShadow];
        
        CGRect frame1 = CGRectMake((scrollFontStyle.frame.size.width /2) + 20, 30, scrollFontStyle.frame.size.width/2 - 30, 20);
        sliderFontShadow = [[UISlider alloc] initWithFrame:frame1];
        sliderFontShadow.minimumTrackTintColor = [UIColor whiteColor];
        sliderFontShadow.maximumTrackTintColor = [UIColor grayColor];
        [sliderFontShadow setThumbImage:[UIImage imageNamed:@"sliderImg"] forState:UIControlStateNormal];
        [sliderFontShadow addTarget:self action:@selector(sliderFontShadowAction:) forControlEvents:UIControlEventValueChanged];
        [sliderFontShadow setBackgroundColor:[UIColor clearColor]];
        sliderFontShadow.minimumValue = 0.0;
        sliderFontShadow.maximumValue = 10;
        sliderFontShadow.continuous = YES;
        sliderFontShadow.value = 0.0;
        [scrollFontStyle addSubview:sliderFontShadow];
        
        btnShadowAngleRight = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnShadowAngleRight addTarget:self
                    action:@selector(btnShadowAngleRightPressed:)
          forControlEvents:UIControlEventTouchUpInside];
        btnShadowAngleRight.frame = CGRectMake(10, 55, 30, 30);
        [btnShadowAngleRight setImage:[UIImage imageNamed:@"ic_FontRight"] forState:UIControlStateNormal];
        [btnShadowAngleRight setImage:[UIImage imageNamed:@"ic_FontRightSele"] forState:UIControlStateSelected];
        [btnShadowAngleRight setSelected:YES];
        [scrollFontStyle addSubview:btnShadowAngleRight];
        
        btnShadowAngleBotton = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnShadowAngleBotton addTarget:self
                                action:@selector(btnShadowAngleBottomPressed:)
                      forControlEvents:UIControlEventTouchUpInside];
        btnShadowAngleBotton.frame = CGRectMake((scrollFontStyle.frame.size.width / 3) - 15, 55, 30, 30);
        [btnShadowAngleBotton setImage:[UIImage imageNamed:@"ic_FontDown"] forState:UIControlStateNormal];
        [btnShadowAngleBotton setImage:[UIImage imageNamed:@"ic_FontDownSele"] forState:UIControlStateSelected];
        [scrollFontStyle addSubview:btnShadowAngleBotton];
        
        btnShadowAngleLeft = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnShadowAngleLeft addTarget:self
                                 action:@selector(btnShadowAngleLeftPressed:)
                       forControlEvents:UIControlEventTouchUpInside];
        btnShadowAngleLeft.frame = CGRectMake((scrollFontStyle.frame.size.width / 2) + 30, 55, 30, 30);
        [btnShadowAngleLeft setImage:[UIImage imageNamed:@"ic_FontLeft"] forState:UIControlStateNormal];
        [btnShadowAngleLeft setImage:[UIImage imageNamed:@"ic_FontLeftSele"] forState:UIControlStateSelected];
        [scrollFontStyle addSubview:btnShadowAngleLeft];
        
        btnShadowAngleTop = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnShadowAngleTop addTarget:self
                               action:@selector(btnShadowAngleTopPressed:)
                     forControlEvents:UIControlEventTouchUpInside];
        btnShadowAngleTop.frame = CGRectMake((scrollFontStyle.frame.size.width) - 40, 55, 30, 30);
        [btnShadowAngleTop setImage:[UIImage imageNamed:@"ic_FontUp"] forState:UIControlStateNormal];
        [btnShadowAngleTop setImage:[UIImage imageNamed:@"ic_FontUpSele"] forState:UIControlStateSelected];
        [scrollFontStyle addSubview:btnShadowAngleTop];
        
        
        
        UICollectionViewFlowLayout *layout2=[[UICollectionViewFlowLayout alloc] init];
        collShadowColor =[[UICollectionView alloc] initWithFrame:CGRectMake(0, 90, self.frame.size.width, 50) collectionViewLayout:layout2];
        layout2.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout2.minimumLineSpacing = 5.0f;
        [collShadowColor setShowsHorizontalScrollIndicator:NO];
        [collShadowColor setDataSource:self];
        [collShadowColor setDelegate:self];
        [collShadowColor registerClass:[StickerCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collShadowColor setBackgroundColor:[UIColor clearColor]];
        [scrollFontStyle addSubview:collShadowColor];
        
        [scrollView setContentSize:CGSizeMake(self.frame.size.width*3, scrollView.frame.size.height)];
        
        strColorType = @"textColor";
        
        viewBtn = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 26, self.frame.size.width, 25)];
        viewBtn.backgroundColor = [UIColor clearColor];
        [self addSubview:viewBtn];
        
        UIButton *btnDown = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDown addTarget:self
                    action:@selector(aBtnDownPressed:)
          forControlEvents:UIControlEventTouchUpInside];
        btnDown.frame = CGRectMake(0.0, 1, self.frame.size.width, 25);
        btnDown.backgroundColor = [UIColor clearColor];
        [btnDown setImage:[UIImage imageNamed:@"ic_downArrow"] forState:UIControlStateNormal];
        [viewBtn addSubview:btnDown];
        
        //For 3D rotation
        view3D = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width*3, 0, self.frame.size.width, scrollView.frame.size.height)];
        view3D.backgroundColor = UIColor.clearColor;
        [scrollView addSubview:view3D];
        
        UILabel *lblRotationX = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 140, 30)];
        lblRotationX.text = @"X Rotation :";
        lblRotationX.textAlignment = NSTextAlignmentCenter;
        [lblRotationX setFont:[UIFont fontWithName:FONTROBOTOREGULAR size:15.0]];
        lblRotationX.textColor = UIColor.whiteColor;
        [view3D addSubview:lblRotationX];
        
        CGRect frameX = CGRectMake(10, 30, view3D.frame.size.width/2 - 30, 30.0);
        SliderRotationX = [[UISlider alloc] initWithFrame:frameX];
        SliderRotationX.minimumTrackTintColor = [UIColor whiteColor];
        SliderRotationX.maximumTrackTintColor = [UIColor grayColor];
        [SliderRotationX setThumbImage:[UIImage imageNamed:@"sliderImg"] forState:UIControlStateNormal];
        [SliderRotationX addTarget:self action:@selector(setSliderRotationX:) forControlEvents:UIControlEventValueChanged];
        [SliderRotationX setBackgroundColor:[UIColor clearColor]];
        SliderRotationX.minimumValue = -45.0;
        SliderRotationX.maximumValue = 45.0;
        SliderRotationX.continuous = YES;
        SliderRotationX.value = 0.0;
        [view3D addSubview:SliderRotationX];
        
        UILabel *lblRotationY = [[UILabel alloc] initWithFrame:CGRectMake((view3D.frame.size.width /2) + 20, 0, 140, 30)];
        lblRotationY.text = @"Y Rotation :";
        lblRotationY.textAlignment = NSTextAlignmentCenter;
        [lblRotationY setFont:[UIFont fontWithName:FONTROBOTOREGULAR size:15.0]];
        lblRotationY.textColor = UIColor.whiteColor;
        [view3D addSubview:lblRotationY];
        
        CGRect frameY = CGRectMake((view3D.frame.size.width /2) + 15, 30, view3D.frame.size.width/2 - 30, 30);
        SliderRotationY = [[UISlider alloc] initWithFrame:frameY];
        SliderRotationY.minimumTrackTintColor = [UIColor whiteColor];
        SliderRotationY.maximumTrackTintColor = [UIColor grayColor];
        [SliderRotationY setThumbImage:[UIImage imageNamed:@"sliderImg"] forState:UIControlStateNormal];
        [SliderRotationY addTarget:self action:@selector(setSliderRotationY:) forControlEvents:UIControlEventValueChanged];
        [SliderRotationY setBackgroundColor:[UIColor clearColor]];
        SliderRotationY.minimumValue = -45.0;
        SliderRotationY.maximumValue = 45.0;
        SliderRotationY.continuous = YES;
        SliderRotationY.value = 0.0;
        [view3D addSubview:SliderRotationY];
        
        lblLineSpacing = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, 140, 30)];
        lblLineSpacing.text = @"Line Spacing :";
        lblLineSpacing.textAlignment = NSTextAlignmentCenter;
        [lblLineSpacing setFont:[UIFont fontWithName:FONTROBOTOREGULAR size:15.0]];
        lblLineSpacing.textColor = UIColor.whiteColor;
        [view3D addSubview:lblLineSpacing];
        
        CGRect frameLine = CGRectMake(10, 100, view3D.frame.size.width/2 - 30, 30);
        SliderLineSpacing = [[UISlider alloc] initWithFrame:frameLine];
        SliderLineSpacing.minimumTrackTintColor = [UIColor whiteColor];
        SliderLineSpacing.maximumTrackTintColor = [UIColor grayColor];
        [SliderLineSpacing setThumbImage:[UIImage imageNamed:@"sliderImg"] forState:UIControlStateNormal];
        [SliderLineSpacing addTarget:self action:@selector(setSliderLineSpace:) forControlEvents:UIControlEventValueChanged];
        [SliderLineSpacing setBackgroundColor:[UIColor clearColor]];
        SliderLineSpacing.minimumValue = 0;
        SliderLineSpacing.maximumValue = 50;
        SliderLineSpacing.continuous = YES;
        SliderLineSpacing.value = 0.0;
        [view3D addSubview:SliderLineSpacing];
        
        lblWordsSpacing = [[UILabel alloc] initWithFrame:CGRectMake((view3D.frame.size.width /2) + 20, 70, 140, 30)];
        lblWordsSpacing.text = @"Word Spacing :";
        lblWordsSpacing.textAlignment = NSTextAlignmentCenter;
        [lblWordsSpacing setFont:[UIFont fontWithName:FONTROBOTOREGULAR size:15.0]];
        lblWordsSpacing.textColor = UIColor.whiteColor;
        [view3D addSubview:lblWordsSpacing];
        
        CGRect frameWord = CGRectMake((view3D.frame.size.width /2) + 15, 100, view3D.frame.size.width/2 - 30, 30);
        SliderWordSpacing = [[UISlider alloc] initWithFrame:frameWord];
        SliderWordSpacing.minimumTrackTintColor = [UIColor whiteColor];
        SliderWordSpacing.maximumTrackTintColor = [UIColor grayColor];
        [SliderWordSpacing setThumbImage:[UIImage imageNamed:@"sliderImg"] forState:UIControlStateNormal];
        [SliderWordSpacing addTarget:self action:@selector(setSliderWordSpace:) forControlEvents:UIControlEventValueChanged];
        [SliderWordSpacing setBackgroundColor:[UIColor clearColor]];
        SliderWordSpacing.minimumValue = -5;
        SliderWordSpacing.maximumValue = 50;
        SliderWordSpacing.continuous = YES;
        SliderWordSpacing.value = 0;
        [view3D addSubview:SliderWordSpacing];
        
        viewTextOutline = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width*4, 0, self.frame.size.width, scrollView.frame.size.height)];
        viewTextOutline.backgroundColor = UIColor.clearColor;
        [scrollView addSubview:viewTextOutline];
        
        lblTextStrock = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 140, 30)];
        lblTextStrock.text = @"Strock :";
        lblTextStrock.textAlignment = NSTextAlignmentCenter;
        [lblTextStrock setFont:[UIFont fontWithName:FONTROBOTOREGULAR size:15.0]];
        lblTextStrock.textColor = UIColor.whiteColor;
        [viewTextOutline addSubview:lblTextStrock];
        
        CGRect frameS = CGRectMake(10, 30, viewTextOutline.frame.size.width/2 - 30, 30.0);
        SliderStrockSize = [[UISlider alloc] initWithFrame:frameS];
        SliderStrockSize.minimumTrackTintColor = [UIColor whiteColor];
        SliderStrockSize.maximumTrackTintColor = [UIColor grayColor];
        [SliderStrockSize setThumbImage:[UIImage imageNamed:@"sliderImg"] forState:UIControlStateNormal];
        [SliderStrockSize addTarget:self action:@selector(setSliderStrockSize:) forControlEvents:UIControlEventValueChanged];
        [SliderStrockSize setBackgroundColor:[UIColor clearColor]];
        SliderStrockSize.minimumValue = 0;
        SliderStrockSize.maximumValue = 5;
        SliderStrockSize.continuous = YES;
        SliderStrockSize.value = 0.0;
        [viewTextOutline addSubview:SliderStrockSize];
        
        lblTextAlpha = [[UILabel alloc] initWithFrame:CGRectMake((viewTextOutline.frame.size.width /2) + 20, 0, 130, 30)];
        lblTextAlpha.text = @"Text Opacity :";
        lblTextAlpha.textAlignment = NSTextAlignmentCenter;
        [lblTextAlpha setFont:[UIFont fontWithName:FONTROBOTOREGULAR size:15.0]];
        lblTextAlpha.textColor = UIColor.whiteColor;
        [viewTextOutline addSubview:lblTextAlpha];
        
        CGRect frameA = CGRectMake((viewTextOutline.frame.size.width /2) + 20, 30, viewTextOutline.frame.size.width/2 - 30, 30);
        SliderTextAlpha = [[UISlider alloc] initWithFrame:frameA];
        SliderTextAlpha.minimumTrackTintColor = [UIColor whiteColor];
        SliderTextAlpha.maximumTrackTintColor = [UIColor grayColor];
        [SliderTextAlpha setThumbImage:[UIImage imageNamed:@"sliderImg"] forState:UIControlStateNormal];
        [SliderTextAlpha addTarget:self action:@selector(setTextAlpha:) forControlEvents:UIControlEventValueChanged];
        [SliderTextAlpha setBackgroundColor:[UIColor clearColor]];
        SliderTextAlpha.minimumValue = 0.2;
        SliderTextAlpha.maximumValue = 1;
        SliderTextAlpha.continuous = YES;
        SliderTextAlpha.value = 1.0;
        [viewTextOutline addSubview:SliderTextAlpha];
        
        
        UICollectionViewFlowLayout *layoutS=[[UICollectionViewFlowLayout alloc] init];
        [layoutS setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        collStrockColor =[[UICollectionView alloc] initWithFrame:CGRectMake(0, 70, self.frame.size.width, 80) collectionViewLayout:layoutS];
        layoutS.minimumLineSpacing = 10.0f;
        layoutS.minimumInteritemSpacing = 5.0f;
        [collStrockColor setDataSource:self];
        [collStrockColor setDelegate:self];
        [collStrockColor registerClass:[StickerCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collStrockColor setBackgroundColor:[UIColor clearColor]];
        [viewTextOutline addSubview:collStrockColor];
        
        
        viewTextAngle = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width*5, 0, self.frame.size.width, scrollView.frame.size.height)];
        viewTextAngle.backgroundColor = UIColor.clearColor;
        [scrollView addSubview:viewTextAngle];
        
        lblTextAngle = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 130, 30)];
        lblTextAngle.text = @"Text Angle :";
        lblTextAngle.textAlignment = NSTextAlignmentLeft;
        [lblTextAngle setFont:[UIFont fontWithName:FONTROBOTOREGULAR size:15.0]];
        lblTextAngle.textColor = UIColor.whiteColor;
        [viewTextAngle addSubview:lblTextAngle];
        
        CGRect frameAB = CGRectMake(140, 10, viewTextAngle.frame.size.width/2 - 30, 30);
        SliderTextAngle = [[UISlider alloc] initWithFrame:frameAB];
        SliderTextAngle.minimumTrackTintColor = [UIColor whiteColor];
        SliderTextAngle.maximumTrackTintColor = [UIColor grayColor];
        [SliderTextAngle setThumbImage:[UIImage imageNamed:@"sliderImg"] forState:UIControlStateNormal];
        [SliderTextAngle addTarget:self action:@selector(setTextAngle:) forControlEvents:UIControlEventValueChanged];
        [SliderTextAngle setBackgroundColor:[UIColor clearColor]];
        SliderTextAngle.minimumValue = -180;
        SliderTextAngle.maximumValue = 180;
        SliderTextAngle.continuous = YES;
        SliderTextAngle.value = 0.0;
        [viewTextAngle addSubview:SliderTextAngle];
        
        
        ColorArray=[NSArray arrayWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"Color" ofType:@"plist"]];
        
//        ColorArray = [[NSMutableArray alloc] initWithObjects:@[@"ffe259",@"ffa751"],@[@"108dc7",@"ef8e38"],@[@"fc5c7d",@"6a82fb"],@[@"FC466B",@"3F5EFB"],@[@"c94b4b",@"4b134f"],@[@"23074d",@"cc5333"],@[@"00b09b",@"96c93d"],@[@"00F260",@"0575E6"], nil];
        
        [collColorView reloadData];
        
        categoryArr = [NSArray arrayWithObjects:@"ic_fontstyle",@"ic_fontColor",@"ic_fontSize",@"ic_3D", nil];
        
        arrFontStyle = [[NSArray alloc] initWithObjects:@"Amadeus",@"CabinSketch-Bold",@"Campanile",@"Captureit",@"UVFDKCrayonCrumble",@"VNFAdobeCaslonPro",@"DroidSansMono",@"UnfinishedSympathy",@"TremoloFlaw",@"DutchTulipsDemo",@"Esquisito",@"FloralCapsNouveau",@"FicticciaCollege",@"Firebug",@"MailartRubberstamp",@"MarketDeco",@"MyBigHeartDemo",@"Payday",@"SablonWashed",@"Sancreek-Regular",@"Sesame-Regular",@"SketchHandwriting",@"UnfinishedSympathy",@"TremoloFlaw",@"TWINPINES",@"TYPOSKETCHDEMO",@"UVFValentine",nil];
        
        StrockColor = [UIColor whiteColor];
        
        [tblFonts reloadData];
    }
    
    return self;
}

-(void)aBtnDownPressed:(UIButton*)sender
{
    //NSLog(@"you clicked on button %@", sender.tag);
    [delegate hideFontStyleTool];
}

-(IBAction)sliderFontSizeAction:(id)sender
{
    float val =  sliderFontSize.value;
    [delegate selectedFontSize:val];
}

-(IBAction)sliderFontShadowAction:(id)sender
{
    float val =  sliderFontShadow.value;
    if(btnShadowAngleRight.isSelected)
    {
        [delegate selectedFontShadow:val Type:1];
    }
    else if(btnShadowAngleBotton.isSelected)
    {
        [delegate selectedFontShadow:val Type:2];
    }
    else if(btnShadowAngleLeft.isSelected)
    {
        [delegate selectedFontShadow:val Type:3];
    }
    else if(btnShadowAngleTop.isSelected)
    {
        [delegate selectedFontShadow:val Type:4];
    }
}


- (void)onSliderValChanged:(UISlider*)slider forEvent:(UIEvent*)event
{
    UITouch *touchEvent = [[event allTouches] anyObject];
    float val =  slider.value;
    switch (touchEvent.phase) {
        case UITouchPhaseBegan:
            // handle drag began
            break;
        case UITouchPhaseMoved:
            // handle drag moved
                [delegate selectedFontSize:val];
            break;
        case UITouchPhaseEnded:
            // handle drag ended
            break;
        default:
            break;
    }
}

-(void)setSliderRotationX:(UISlider *)SliderRotationX
{
    float val =  SliderRotationX.value;
    SliderRotationY.value = 0.0;
    [delegate selectedRotationX:val];
}

-(void)setSliderRotationY:(UISlider *)SliderRotationY
{
    float val =  SliderRotationY.value;
    SliderRotationX.value = 0.0;
    [delegate selectedRotationY:val];
}

-(void)setSliderLineSpace:(UISlider *)SliderLineSpace
{
    float val =  SliderLineSpacing.value;
    [delegate setLineSpacing:val withWordSpacing:SliderWordSpacing.value];
}

-(void)setSliderWordSpace:(UISlider *)SliderLineSpace
{
    float val =  SliderWordSpacing.value;
    [delegate setWordSpacing:val withLineSpeacing:SliderLineSpacing.value];
}

-(void)setSliderBlurShadow:(UISlider *)SliderBlurShadow
{
    float val =  SliderBlurShadow.value;
    //[delegate setLineSpacing:val withWordSpacing:SliderWordSpacing.value];
    [delegate selectedBlurShadow:val];
}

-(void)setSliderStrockSize:(UISlider *)SliderStrockSize
{
    float val = SliderStrockSize.value;
    [delegate selectedStrockSize:val StrockColor:StrockColor];
}

-(void)setTextAlpha:(UISlider *)Alpha
{
    float val = Alpha.value;
    [delegate setTextAlpha:val];
}

-(void)setTextAngle:(UISlider *)Angle
{
    NSString* Textangle = [NSString stringWithFormat:@"%.f", Angle.value];
    
    float val = [Textangle floatValue];
    [delegate setTextAngle:val];
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    if ([strFontType isEqualToString:@"Font"]) {
         return arrFontStyle.count;
    }
    else {
         return selectedFontCategoryArr.count;
    }
    
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"FontCell";
    
    // Similar to UITableViewCell, but
    FontCell *cell = (FontCell *)[tblFonts dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.lblFontName.backgroundColor = [UIColor clearColor];
    if (cell == nil) {
        cell = [[FontCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if ([strFontType isEqualToString:@"Font"]) {
        cell.lblFontName.text = arrFontStyle[indexPath.row];
        cell.lblFontName.textAlignment = NSTextAlignmentCenter;
        cell.lblFontName.textColor = [UIColor whiteColor];
        cell.lblFontName.font = [UIFont fontWithName:arrFontStyle[indexPath.row] size:15];
    }
    else {
        
        cell.lblFontName.text = selectedFontCategoryArr[indexPath.row][@"FontName"];
        cell.lblFontName.textAlignment = NSTextAlignmentCenter;
        cell.lblFontName.textColor = [UIColor redColor];
        cell.lblFontName.font = [UIFont fontWithName:selectedFontCategoryArr[indexPath.row][@"FontName"] size:15];
    }
    
  

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([strFontType isEqualToString:@"Font"]) {
        [delegate selectedFontStyle:arrFontStyle[indexPath.row]];
    }
    else {
        [delegate selectedFontStyle:selectedFontCategoryArr[indexPath.row][@"FontName"]];
    }
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //return colorArr.count;
    if (collectionView == collCategoryView) {
        return categoryArr.count;
    }
    else if (collectionView == collShadowColor)
    {
        return ColorArray.count;
    }
    else {
        return ColorArray.count;
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collCategoryView) {
        CategoryCell *cell = [collCategoryView dequeueReusableCellWithReuseIdentifier:@"cellCategory" forIndexPath:indexPath];
        cell.imgFont.image = [UIImage imageNamed:categoryArr[indexPath.row]];
        cell.lblSelector.frame = CGRectMake(cell.lblSelector.frame.origin.x, cell.lblSelector.frame.origin.y, cell.lblSelector.frame.size.width, 2);
        cell.lblSelector.layer.cornerRadius = 1;
        cell.lblSelector.layer.masksToBounds = YES;
        if (selectedIndex == indexPath.row)
        {
            cell.lblSelector.backgroundColor = [UIColor blackColor];
        }
        else
        {
            cell.lblSelector.backgroundColor = [UIColor clearColor];
        }
        return cell;
    }
    else if (collectionView == collShadowColor)
    {
        StickerCell *cell = [collShadowColor dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
        NSString *strColor = [ColorArray[indexPath.row] substringFromIndex:1];
        
        if (selectedTextShadowColor == strColor)
        {
            cell.imgSelection.image = [UIImage imageNamed:@"ic_selection"];
        }
        else
        {
            cell.imgSelection.image = nil;
        }
        
        cell.imgSticker.backgroundColor = [self colorWithHexString:strColor];
        cell.layer.cornerRadius = 4.0f;
        cell.imgSelection.layer.cornerRadius = 4.0f;
        cell.imgSelection.layer.masksToBounds = YES;
        cell.imgSticker.layer.cornerRadius = 4.0f;
        cell.imgSticker.layer.masksToBounds = YES;
//        cell.layer.shadowRadius  = 4.0f;
//        cell.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
//        cell.layer.shadowOffset  = CGSizeZero;
//        cell.layer.shadowOpacity = 1.2f;
        cell.layer.masksToBounds = NO;
        return cell;
    }
    else if(collectionView == collColorView) {
        StickerCell *cell = [collColorView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
        NSString *strColor = [ColorArray[indexPath.row] substringFromIndex:1];
        
        if (selectedTextColor == strColor)
        {
            cell.imgSelection.image = [UIImage imageNamed:@"ic_selection"];
        }
        else
        {
            cell.imgSelection.image = nil;
        }
        
        cell.imgSticker.backgroundColor = [self colorWithHexString:strColor];
        cell.layer.cornerRadius = 4.0f;
        cell.imgSelection.layer.cornerRadius = 4.0f;
        cell.imgSelection.layer.masksToBounds = YES;
        cell.imgSticker.layer.cornerRadius = 4.0f;
        cell.imgSticker.layer.masksToBounds = YES;
//        cell.layer.shadowRadius  = 4.0f;
//        cell.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
//        cell.layer.shadowOffset  = CGSizeZero;
//        cell.layer.shadowOpacity = 1.2f;
        cell.layer.masksToBounds = NO;
        return cell;
    }
    else
    {
        StickerCell *cell = [collColorView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
        NSString *strColor = [ColorArray[indexPath.row] substringFromIndex:1];
        
        if (selectedStrockColor == strColor)
        {
            cell.imgSelection.image = [UIImage imageNamed:@"ic_selection"];
        }
        else
        {
            cell.imgSelection.image = nil;
        }
        
        cell.imgSticker.backgroundColor = [self colorWithHexString:strColor];
        cell.layer.cornerRadius = 4.0f;
        cell.imgSelection.layer.cornerRadius = 4.0f;
        cell.imgSelection.layer.masksToBounds = YES;
        cell.imgSticker.layer.cornerRadius = 4.0f;
        cell.imgSticker.layer.masksToBounds = YES;
//        cell.layer.shadowRadius  = 4.0f;
//        cell.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
//        cell.layer.shadowOffset  = CGSizeZero;
//        cell.layer.shadowOpacity = 1.2f;
        cell.layer.masksToBounds = NO;
        return cell;
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collCategoryView) {
        return CGSizeMake([[UIScreen mainScreen] bounds].size.width/categoryArr.count, 35);
    }
    else if (collectionView == collShadowColor)
    {
        return CGSizeMake(40, 40);
    }
    else {
        return CGSizeMake(50, 50);
    }
}



- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    if (collectionView == collCategoryView) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    else {
        return UIEdgeInsetsMake(5, 10, 10, 10); // top, left, bottom, right
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == collCategoryView) {
        selectedIndex = indexPath.row;
        [collCategoryView reloadData];
        if (indexPath.row == 0)
        {
//            [delegate addText];
            [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        else if (indexPath.row == 1){
            [scrollView setContentOffset:CGPointMake(self.frame.size.width*1, 0) animated:YES];
        }
        else if (indexPath.row == 2){
            [scrollView setContentOffset:CGPointMake(self.frame.size.width*2, 0) animated:YES];
        }
        else {
            [scrollView setContentOffset:CGPointMake(self.frame.size.width*3, 0) animated:YES];
        }
    }
    else if (collectionView == collShadowColor)
    {
//        StickerCell *cell = (StickerCell*)[collShadowColor cellForItemAtIndexPath:indexPath];
//        cell.imgSelection.image = [UIImage imageNamed:@"ic_selection"];
        NSString *strColor = [ColorArray[indexPath.row] substringFromIndex:1];
        selectedTextShadowColor = strColor;
        [collShadowColor reloadData];
        UIColor *color = [self colorWithHexString:strColor];
        [delegate selectedShadowColor:color];
    }
    else if(collectionView == collColorView) {
//        StickerCell *cell = (StickerCell*)[collColorView cellForItemAtIndexPath:indexPath];
//        cell.imgSelection.image = [UIImage imageNamed:@"ic_selection"];
        NSString *strColor = [ColorArray[indexPath.row] substringFromIndex:1];
        selectedTextColor = strColor;
        [collColorView reloadData];
        UIColor *color = [self colorWithHexString:strColor];
        [delegate selectedColor:color];
    }
    else
    {
        NSString *strColor = [ColorArray[indexPath.row] substringFromIndex:1];
        selectedStrockColor = strColor;
        [collStrockColor reloadData];
        UIColor *color = [self colorWithHexString:strColor];
        StrockColor = color;
        [delegate selectedStrockSize:SliderStrockSize.value StrockColor:color];
    }
}


-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == collCategoryView)
    {
        CategoryCell *cell = (CategoryCell*)[collCategoryView cellForItemAtIndexPath:indexPath];
        [UIView animateWithDuration:2.0 animations:^{
            cell.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1];
        } completion:NULL];
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == collCategoryView)
    {
        CategoryCell *cell = (CategoryCell*)[collCategoryView cellForItemAtIndexPath:indexPath];
        [UIView animateWithDuration:2.0 animations:^{
            cell.backgroundColor = [UIColor clearColor];
        } completion:NULL];
    }
}

- (void)scrollMenuDidSelected:(XHScrollMenu *)scrollMenu menuIndex:(NSUInteger)selectIndex {
    if ([fontCategoryArr[selectIndex] isEqualToString:@"Font"])
    {
        strFontType = @"Font";
        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        [tblFonts reloadData];
    }
    else if ([fontCategoryArr[selectIndex] isEqualToString:@"Text Color"])
    {
        strFontType = @"Text Color";
        [scrollView setContentOffset:CGPointMake(self.frame.size.width*1, 0) animated:YES];
        [collColorView reloadData];
    }
    else if ([fontCategoryArr[selectIndex] isEqualToString:@"Shadow/Size"])
    {
        strFontType = @"Shadow/Size";
        [scrollView setContentOffset:CGPointMake(self.frame.size.width*2, 0) animated:YES];
        [collShadowColor reloadData];
    }
    else if ([fontCategoryArr[selectIndex] isEqualToString:@"3D/Spacing"])
    {
        strFontType = @"3D/Spacing";
        [scrollView setContentOffset:CGPointMake(self.frame.size.width*3, 0) animated:YES];
    }
    else if ([fontCategoryArr[selectIndex] isEqualToString:@"TextStrock"])
    {
        strFontType = @"TextStrock";
        [scrollView setContentOffset:CGPointMake(self.frame.size.width*4, 0) animated:YES];
    }
    else
    {
        strFontType = @"TextAngle";
        [scrollView setContentOffset:CGPointMake(self.frame.size.width*5, 0) animated:YES];
    }
//    else
//    {
//        strFontType = @"Webservice";
//        [selectedFontCategoryArr removeAllObjects];
//        [selectedFontCategoryArr addObjectsFromArray:allFontStyleArr[selectIndex-1][@"value"]];
//        [self getFontListByCategory:selectedFontCategoryArr];
//    }
}

- (void)scrollMenuDidManagerSelected:(XHScrollMenu *)scrollMenu {
    NSLog(@"scrollMenuDidManagerSelected");
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.scrollView)
    {
        if (scrollView.contentOffset.x == 0)
        {
            selectedIndex = 0;
            [collCategoryView reloadData];
        }
        else if (scrollView.contentOffset.x == self.frame.size.width)
        {
            selectedIndex = 1;
            [collCategoryView reloadData];
        }
        else if (scrollView.contentOffset.x == self.frame.size.width*2)
        {
            selectedIndex = 2;
            [collCategoryView reloadData];
        }
        else if (scrollView.contentOffset.x == self.frame.size.width*3)
        {
            selectedIndex = 3;
            [collCategoryView reloadData];
        }
        else if(scrollView.contentOffset.x == self.frame.size.width*4)
        {
            selectedIndex = 4;
            [collStrockColor reloadData];
        }
        else
        {
            selectedIndex = 5;
            
        }
    }
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)setFontCategory {
//    fontCategoryArr = [NSMutableArray arrayWithObjects:@"Default", nil];
    
    fontCategoryArr = [NSMutableArray array];
    [fontCategoryArr addObject:@"Font"];
    [fontCategoryArr addObject:@"Text Color"];
    [fontCategoryArr addObject:@"Shadow/Size"];
    [fontCategoryArr addObject:@"3D/Spacing"];
    [fontCategoryArr addObject:@"TextStrock"];
    [fontCategoryArr addObject:@"TextAngle"];
    
    _scrollMenu.menus = fontCategoryArr;
    _scrollMenu.shouldUniformizeMenus = YES;
    
    _scrollMenu.frame = CGRectMake(0, 0, self.frame.size.width, 40);
    _scrollMenu.selectedIndex = 0;
    strFontType = @"Font";
    
    selectedIndex = 0;
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    [tblFonts reloadData];
    
    [_scrollMenu reloadData];
    
}

-(void)getFontListByCategory:(NSMutableArray*)arrFont
{
    
    for (int j = 0; j < arrFont.count; j++){
        
        NSString *str;
        if ([self isFontDownloaded:arrFont[j][@"font"]]){
            str = [self realNameOfFont:arrFont[j][@"font"]];
            NSLog(@"%@",str);
        }
        else{
            [self downloadAndSaveFontFile:arrFont[j][@"url"] withFontFile:arrFont[j][@"font"]];
        }
    }
    

    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSMutableArray *tempArry = [NSMutableArray array];
        for (int j = 0; j < arrFont.count; j++){
            
            NSString *str;
            if ([self isFontDownloaded:arrFont[j][@"font"]]){
                str = [self realNameOfFont:arrFont[j][@"font"]];
                NSLog(@"%@",str);
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:arrFont[j]];
                [dict setObject:str forKey:@"FontName"];
                //                [dict setValue:str forKey:@"FontName"];
                
                [arrFont replaceObjectAtIndex:j withObject:dict];
            }
        }
        
        //
        //  NSLog(@"arr Font : %@",selectedFontCategoryArr);
        NSLog(@"arr Font : %@",arrFont);
        [tempArry addObjectsFromArray:arrFont];
        [selectedFontCategoryArr removeAllObjects];
        [selectedFontCategoryArr addObjectsFromArray:tempArry];
        [tblFonts reloadData];

    });
    
}

-(BOOL)isFontDownloaded:(NSString*)fontFileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *docFilePath = [documentsDirectory stringByAppendingPathComponent:fontFileName];
    
    NSData *data = [NSData dataWithContentsOfFile:docFilePath];
    if (data == nil) {
        NSLog(@"Failed to load font. Data at path %@ is null", fontFileName);
        return false;
    }
    else{
        return true;
    }
}

-(void)btnShadowAngleBottomPressed:(UIButton*)sender
{
    [sender setSelected:YES];
    [btnShadowAngleRight setSelected:NO];
    [btnShadowAngleTop setSelected:NO];
    [btnShadowAngleLeft setSelected:NO];
    [delegate selectedFontShadow:sliderFontShadow.value Type:2];
}

-(void)btnShadowAngleTopPressed:(UIButton*)sender
{
    [sender setSelected:YES];
    [btnShadowAngleBotton setSelected:NO];
    [btnShadowAngleRight setSelected:NO];
    [btnShadowAngleLeft setSelected:NO];
    [delegate selectedFontShadow:sliderFontShadow.value Type:4];
}

-(void)btnShadowAngleLeftPressed:(UIButton*)sender
{
    [sender setSelected:YES];
    [btnShadowAngleBotton setSelected:NO];
    [btnShadowAngleTop setSelected:NO];
    [btnShadowAngleRight setSelected:NO];
    [delegate selectedFontShadow:sliderFontShadow.value Type:3];
}

-(void)btnShadowAngleRightPressed:(UIButton*)sender
{
    NSLog(@"you clicked on button %@", sender.tag);
   
    [sender setSelected:YES];
    [btnShadowAngleBotton setSelected:NO];
    [btnShadowAngleTop setSelected:NO];
    [btnShadowAngleLeft setSelected:NO];
    [delegate selectedFontShadow:sliderFontShadow.value Type:1];
    
}

-(void)btnAlignLeftPressed:(UIButton*)sender
{
    [sender setSelected:YES];
    [btnAlignCenter setSelected:NO];
    [btnAlignRight setSelected:NO];
    [delegate selectedTextAlignment:NSTextAlignmentLeft];
    
}

-(void)btnAlignCenterPressed:(UIButton*)sender
{
    [sender setSelected:YES];
    [btnAlignLeft setSelected:NO];
    [btnAlignRight setSelected:NO];
    [delegate selectedTextAlignment:NSTextAlignmentCenter];
}

-(void)btnAlignRightPressed:(UIButton*)sender
{
    [sender setSelected:YES];
    [btnAlignLeft setSelected:NO];
    [btnAlignCenter setSelected:NO];
    [delegate selectedTextAlignment:NSTextAlignmentRight];
}
//MARK:- Download Fonts
-(void)downloadAndSaveFontFile:(NSString*)strUrl withFontFile:(NSString*)fontFile {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:strUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
        //  [self getFontName:fontFile];
    }];
    [downloadTask resume];
    
}


//MARK: Font Register
- (NSString *)realNameOfFont:(NSString *)fullFontName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *docFilePath = [documentsDirectory stringByAppendingPathComponent:fullFontName];
    
    NSData *data = [NSData dataWithContentsOfFile:docFilePath];
    if (data == nil) {
        NSLog(@"Failed to load font. Data at path %@ is null", fullFontName);
        return nil;
    }
    CFErrorRef errorRef;
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)data);
    CGFontRef font = CGFontCreateWithDataProvider(provider);
    NSString *realFontName = (__bridge NSString *)CGFontCopyPostScriptName(font);
    if (!CTFontManagerRegisterGraphicsFont(font, &errorRef)) {
        NSError *error = (__bridge NSError *)errorRef;
        if (error.code != kCTFontManagerErrorAlreadyRegistered) {
            NSLog(@"Failed to load font: %@", error);
        }
    }
    NSLog(@"PostScript Font Name:%@", realFontName);
    CFRelease(font);
    CFRelease(provider);
    return realFontName;
}



@end
