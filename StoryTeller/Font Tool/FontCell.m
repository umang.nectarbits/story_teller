//
//  FontCell.m
//  Typogrphy
//
//  Created by NectarBits on 05/01/18.
//  Copyright © 2018 NectarBits. All rights reserved.
//

#import "FontCell.h"


@implementation FontCell
@synthesize lblFontName;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // configure control(s)
        
        lblFontName = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, [[UIScreen mainScreen] bounds].size.width-10, 30)];
        lblFontName.textColor = [UIColor blackColor];
        [lblFontName setTranslatesAutoresizingMaskIntoConstraints:NO];
        lblFontName.font = [UIFont fontWithName:@"Arial" size:12.0f];
        
        [self addSubview:lblFontName];
        
        _lblSeperator = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height-1, [[UIScreen mainScreen] bounds].size.width, 1)];
        _lblSeperator.backgroundColor = [UIColor lightGrayColor];
        [_lblSeperator setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self addSubview:_lblSeperator];
    }
    return self;
}

/*
- (instancetype)initWithFrame:(CGRect)rect {
    
    self = [super initWithFrame:rect];
    
    if (self) {
        lblFontName = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, rect.size.width-10, rect.size.height-10)];
        lblFontName.clipsToBounds = YES;
        [lblFontName setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.contentView addSubview:lblFontName];
    }
    
    
    return self;
}*/


@end
