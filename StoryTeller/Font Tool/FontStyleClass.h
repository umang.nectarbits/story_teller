//
//  FontStyleClass.h
//  Typogrphy
//
//  Created by NectarBits on 05/01/18.
//  Copyright © 2018 NectarBits. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XHMenu.h"
#import "XHScrollMenu.h"
@protocol FontStyleClassDelegate;

@interface FontStyleClass : UIView <UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate,XHScrollMenuDelegate>{
    NSInteger selectedIndex;
    NSInteger selectedFontIndex;
    NSString *selectedTextColor;
    NSString *selectedTextShadowColor;
    NSString *selectedStrockColor;
    UIColor *StrockColor;
}

@property (nonatomic, strong)UICollectionView *collCategoryView;
@property (nonatomic, strong)UICollectionView *collFontCategoryView;
@property (nonatomic, strong) XHScrollMenu *scrollMenu;
@property (nonatomic, strong)UICollectionView *collShadowColor;
@property (nonatomic, strong)UICollectionView *collColorView;
@property (nonatomic, strong)UICollectionView *collStrockColor;

@property (nonatomic, strong)UIScrollView *scrollView;
@property (nonatomic, strong)UIScrollView *scrollFontStyle;
@property (nonatomic, strong)UIView *viewFontStyle;
@property (nonatomic, strong)UIView *viewFontColor;
@property (nonatomic, strong)UIView *viewFontEdit;
@property (nonatomic, strong)UITableView *tblFonts;
@property (nonatomic, strong)UIView *viewSlider;
@property (nonatomic, strong)UISlider *sliderFontSize;
@property (nonatomic, strong)UISlider *sliderFontShadow;
@property (nonatomic, strong)UIView *viewBtn;
@property (nonatomic, strong)NSArray *categoryArr;
@property (nonatomic, strong)NSMutableArray *fontCategoryArr;
@property (nonatomic, strong)NSMutableArray *selectedFontCategoryArr;
@property (nonatomic, strong)NSMutableArray *selectedCategoryArr;
@property (nonatomic, strong)NSArray *ColorArray;
@property (nonatomic, strong)NSArray *arrFontStyle;
@property (nonatomic, strong)NSArray *allFontStyleArr;
@property (nonatomic, strong)NSString *strColorType;
@property (nonatomic, strong)NSString *strFontType;
@property (nonatomic, strong)UISlider *SliderRotationX;
@property (nonatomic, strong)UISlider *SliderRotationY;
@property (nonatomic, strong)UIView *view3D;
@property (nonatomic, strong) UIButton *btnShadowAngleLeft;
@property (nonatomic, strong) UIButton *btnShadowAngleBotton;
@property (nonatomic, strong) UIButton *btnShadowAngleRight;
@property (nonatomic, strong) UIButton *btnShadowAngleTop;
@property (nonatomic, strong)UIView *viewTextOutline;
@property (nonatomic, strong)UILabel *lblTextStrock;
@property (nonatomic, strong)UILabel *lblTextAlpha;
@property (nonatomic, strong)UISlider *SliderStrockSize;
@property (nonatomic, strong)UISlider *SliderTextAlpha;

@property (nonatomic, strong) UIButton *btnAlignLeft;
@property (nonatomic, strong) UIButton *btnAlignCenter;
@property (nonatomic, strong) UIButton *btnAlignRight;

@property (nonatomic, strong)UILabel *lblLineSpacing;
@property (nonatomic, strong)UILabel *lblWordsSpacing;
@property (nonatomic, strong)UISlider *SliderLineSpacing;
@property (nonatomic, strong)UISlider *SliderWordSpacing;
@property (nonatomic, strong)UILabel *lblBlurShadow;
@property (nonatomic, strong)UISlider *SliderBlurShadow;

@property (nonatomic, strong)UIView *viewTextAngle;
@property (nonatomic, strong)UILabel *lblTextAngle;
@property (nonatomic, strong)UISlider *SliderTextAngle;

@property (nonatomic, weak) id<FontStyleClassDelegate> delegate;

-(void)setFontCategory;

@end

@protocol FontStyleClassDelegate <NSObject>
-(void)hideFontStyleTool;
-(void)addText;
-(void)selectedColor:(UIColor *)color;
-(void)selectedShadowColor:(UIColor *)colorShadow;
-(void)selectedFontStyle:(NSString *)strFont;
-(void)selectedFontSize:(float )selectedFontSize;
-(void)selectedFontShadow:(float )fontShadow Type:(int)Angle;
-(void)selectedRotationX:(float )txtRotationX;
-(void)selectedRotationY:(float )txtRotationY;
-(void)setLineSpacing:(float)spacing withWordSpacing:(float)wordSpacing;
-(void)setWordSpacing:(float)Wordspacing withLineSpeacing:(float)lineSpacing;
-(void)selectedBlurShadow:(float )BlurRadius;
-(void)selectedTextAlignment:(NSTextAlignment)Alignment;
-(void)selectedStrockSize:(float )Strock StrockColor:(UIColor *)color;
-(void)setTextAlpha:(float)alpha;
-(void)setTextAngle:(float)angle;

@end


