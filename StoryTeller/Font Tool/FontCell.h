//
//  FontCell.h
//  Typogrphy
//
//  Created by NectarBits on 05/01/18.
//  Copyright © 2018 NectarBits. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FontCell : UITableViewCell

@property(nonatomic,strong) UILabel *lblFontName;
@property(nonatomic,strong) UILabel *lblSeperator;

@end
